[metadata]
name = cki-tools
description = "CKI command line tools"
long_description = file: README.md
version = 1
author = Red Hat, Inc.
license = GPLv2+

[options]
packages =
    cki/beaker_tools
    cki/cki_tools
    cki/cki_tools/amqp_bridge
    cki/cki_tools/service_metrics
    cki/cki_tools/url_shortener
    cki/cki_tools/webhook_receiver
    cki/deployment_tools
    cki/deployment_tools/grafana
    cki/kcidb
    cki/monitoring_tools
    cki/triager
install_requires =
    anymarkup
    boto3
    cached-property
    cki-lib @ git+https://gitlab.com/cki-project/cki-lib.git@production
    datawarehouse-api-lib @ git+https://gitlab.com/cki-project/datawarehouse-api-lib.git@production
    kcidb-io @ git+https://github.com/kernelci/kcidb-io.git@v3
    GitPython
    kubernetes
    python-dateutil
    python-gitlab
    PyYAML
    requests
    sentry-sdk
scripts =
    shell-scripts/cki_deployment_acme.sh
    shell-scripts/cki_deployment_clean_docker_images.sh
    shell-scripts/cki_deployment_codeowners_mr.sh
    shell-scripts/cki_deployment_grafana_backup.sh
    shell-scripts/cki_deployment_grafana_mr.sh
    shell-scripts/cki_deployment_osp_backup.sh
    shell-scripts/cki_deployment_pgsql_backup.sh
    shell-scripts/cki_deployment_pgsql_restore.sh
    shell-scripts/cki_deployment_git_s3_sync.sh
    shell-scripts/cki_tools_git_cache_updater.sh
    shell-scripts/cki_tools_kernel_config_updater.sh

[options.extras_require]
dev =
    freezegun
    kcidb @ git+https://github.com/kernelci/kcidb.git@v9.1
    mypy
    prometheus-client
    responses
    types-PyYAML
    urllib3-mock
    # If sentry-sdk is already installed, the [flask] extra dependencies are not installed.
    # Workaround by forcing the dependencies to be installed.
    Flask<2.2.0
    blinker>=1.1
webhook_receiver =
    # If sentry-sdk is already installed, the [flask] extra dependencies are not installed.
    # Workaround by forcing the dependencies to be installed.
    Flask<2.2.0
    gunicorn
    sentry-sdk[flask]
brew =
    cki-lib[brew] @ git+https://gitlab.com/cki-project/cki-lib.git@production
    # If cki-lib is already installed, the [brew] extra dependencies are not installed.
    # Workaround by forcing koji to be installed.
    koji
kcidb =
    koji
    kcidb @ git+https://github.com/kernelci/kcidb.git@v9.1
    prometheus-client
deployment_tools =
    hvac
    Jinja2
amqp_bridge =
    cli-proton-python
    cki-lib[crypto] @ git+https://gitlab.com/cki-project/cki-lib.git@production
    # If cki-lib is already installed, the [crypto] extra dependencies are not installed.
    # Workaround by forcing cryptography to be installed.
    cryptography
    prometheus-client
autoscaler =
    openshift-client
deployment_bot =
    prometheus-client
k8s_event_listener =
    prometheus-client
url_shortener =
    # If sentry-sdk is already installed, the [flask] extra dependencies are not installed.
    # Workaround by forcing the dependencies to be installed.
    Flask<2.2.0
    gunicorn
    prometheus-client
    sentry-sdk[flask]
triager =
    prometheus-client
service_metrics =
    crontab
    prometheus-client
    cki-lib[psql] @ git+https://gitlab.com/cki-project/cki-lib.git@production
    # If cki-lib is already installed, the [psql] extra dependencies are not installed.
    # Workaround by forcing psycopg2 to be installed.
    psycopg2
    requests-gssapi
beaker_broken_machines =
    cki-lib[psql] @ git+https://gitlab.com/cki-project/cki-lib.git@production
    # If cki-lib is already installed, the [psql] extra dependencies are not installed.
    # Workaround by forcing psycopg2 to be installed.
    psycopg2
refresh_certificates =
    dogtag-pki
    python-ldap  # explicit dependency currently missing from dogtag-pki
    pyOpenSSL
datawarehouse_umb_submitter =
    prometheus-client
gitlab_sso_login =
    requests-gssapi
datawarehouse_utils =
    python-bugzilla

[options.packages.find]
exclude = tests

[options.entry_points]
console_scripts =
    cki_secret = cki.deployment_tools.secrets:_secret_cli
    cki_variable = cki.deployment_tools.secrets:_variable_cli
    cki_encrypt = cki.deployment_tools.secrets:_encrypt_cli

[tox:tox]

[testenv]
passenv =
    MINIO_URL
    MONGODB_HOST
    MONGODB_USERNAME
    MONGODB_PASSWORD
extras =
    amqp_bridge
    autoscaler
    brew
    refresh_certificates
    datawarehouse_utils
    deployment_bot
    deployment_tools
    dev
    gitlab_sso_login
    service_metrics
    webhook_receiver
commands = cki_lint.sh cki

[mypy]
files = cki/cki_tools/yaml.py

[mypy-cki_lib]
# missing type hints/library stub
ignore_missing_imports = True

[flake8]
exclude=.direnv,.git,.tox*

[pylint.MAIN]
extension-pkg-whitelist=falcon

[SIMILARITIES]
ignore-imports=yes
