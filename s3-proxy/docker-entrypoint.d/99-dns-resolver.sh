#!/bin/sh

echo "resolver $(grep nameserver /etc/resolv.conf | cut -d' ' -f2 | xargs);" > /etc/nginx/conf.d/resolver.conf
