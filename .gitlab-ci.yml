---
include:
  - project: cki-project/cki-lib
    file: .gitlab/ci_templates/cki-common.yml

shellcheck:
  extends: .shellcheck

run unittest:
  extends: .tox
  services:
    # docker.io/minio/minio:latest
    - name: quay.io/cki/mirror_minio:latest
      command: ['server', '/minio']
      alias: minio
    - name: quay.io/cki/mirror_mongo:latest
      alias: mongodb
  variables:
    MINIO_URL: 'http://minio:9000'
    MINIO_ACCESS_KEY: user
    MINIO_SECRET_KEY: password
    MONGODB_HOST: mongodb
    MONGODB_USERNAME: root
    MONGODB_PASSWORD: root
    # Required for the mongodb service
    MONGO_INITDB_ROOT_USERNAME: ${MONGODB_USERNAME}
    MONGO_INITDB_ROOT_PASSWORD: ${MONGODB_PASSWORD}

build webhook-lambda:
  extends: .cki_tools
  # docker.io/lambci/lambda:build-python3.8
  image: quay.io/cki/mirror_lambda:build-python3.8
  script:
    - cki/cki_tools/webhook_receiver/build_lambda.sh
  artifacts:
    paths:
      - webhook_receiver_lambda.zip

webhook-lambda:
  extends: .cki_tools
  stage: 🚀
  script:
    - echo deploying webhook zip file
  dependencies:
    - build webhook-lambda
  artifacts:
    paths:
      - webhook_receiver_lambda.zip
  rules: !reference [.deploy_production, rules]

.images:
  parallel:
    matrix:
      - IMAGE_NAME: amqp-bridge
        CHANGES: cki/cki_tools/amqp_bridge
      - IMAGE_NAME: autoscaler
        CHANGES: cki/deployment_tools/autoscaler
      - IMAGE_NAME: cki-tools
        TAG_BRANCH: 'true'
      - IMAGE_NAME: deployment-bot
        CHANGES: cki/deployment_tools/deployment_bot
      - IMAGE_NAME: datawarehouse-kcidb-forwarder
        CHANGES: cki/kcidb
      - IMAGE_NAME: datawarehouse-triager
        CHANGES: cki/triager
      - IMAGE_NAME: datawarehouse-submitter
        CHANGES: cki/{kcidb,beaker_tools}
      - IMAGE_NAME: datawarehouse-umb-submitter
        CHANGES: cki/kcidb/datawarehouse_umb_submitter.py
      - IMAGE_NAME: k8s-event-listener
        CHANGES: cki/cki_tools/k8s_event_listener
      - IMAGE_NAME: s3-proxy
        CHANGES: s3-proxy
      - IMAGE_NAME: service-metrics
        CHANGES: cki/cki_tools/service_metrics
      - IMAGE_NAME: url-shortener
        CHANGES: cki/cki_tools/url_shortener
      - IMAGE_NAME: webhook-receiver
        CHANGES: cki/cki_tools/webhook_receiver

publish:
  extends: [.publish, .images]

s3-proxy-integration-tests:
  extends: .tox
  stage: 💉
  services:
    # docker.io/minio/minio:latest
    - name: quay.io/cki/mirror_minio:latest
      command: [server, /minio]
      alias: minio
    - name: quay.io/cki/s3-proxy:p-$CI_PIPELINE_ID
      alias: s3-proxy
  variables:
    # minio
    MINIO_ROOT_USER: user
    MINIO_ROOT_PASSWORD: password
    MINIO_HOST: minio
    # proxy
    AWS_ACCESS_KEY_ID: user
    AWS_SECRET_ACCESS_KEY: password
    AWS_ENDPOINT_HOST: minio:9000
    AWS_ENDPOINT_SCHEME: http
    AWS_DEFAULT_REGION: us-east-1
    PROXY_HOST: s3-proxy
    # enable container-container networking
    FF_NETWORK_PER_BUILD: "true"
  script:
    - s3-proxy/integration-tests.sh
  rules:
    # not for multi-project pipelines as container images are not rebuilt in
    # dependent pipelines
    - if: $CI_PIPELINE_SOURCE == 'pipeline'
      when: never
    - when: on_success

tag:
  extends: [.tag, .images]

deploy-production:
  extends: [.deploy_production_image, .images]

# review environments

deploy-mr-cki-tools:
  extends: .deploy_mr_image
  variables: {IMAGE_NAME: cki-tools}
  environment: {on_stop: stop-mr-cki-tools}

stop-mr-cki-tools:
  extends: [deploy-mr-cki-tools, .stop_mr]

deploy-mr-deployment-bot:
  extends: .deploy_mr_image
  variables: {IMAGE_NAME: deployment-bot}
  environment: {on_stop: stop-mr-deployment-bot}

stop-mr-deployment-bot:
  extends: [deploy-mr-deployment-bot, .stop_mr]

deploy-mr-url-shortener:
  extends: .deploy_mr_image
  variables: {IMAGE_NAME: url-shortener}
  environment: {on_stop: stop-mr-url-shortener}

stop-mr-url-shortener:
  extends: [deploy-mr-url-shortener, .stop_mr]
