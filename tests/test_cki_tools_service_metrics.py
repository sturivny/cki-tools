import datetime
import json
import os
import unittest
from unittest import mock

from cki_lib.cronjob import CronJob
from freezegun import freeze_time
import responses
import urllib3_mock
import yaml

from cki.cki_tools import service_metrics
from cki.cki_tools.service_metrics import metrics
from cki.cki_tools.service_metrics.metrics import sentry

URLLIB3_MOCK = urllib3_mock.Responses('requests.packages.urllib3')

BEAKER_CONFIG = yaml.safe_load("""
---
beaker_url: https://beaker.com

pools:
  - pool-1
  - pool-2

queues:
  aarch64:
  x86_64:
  i386: [i386, x86_64]
  ppc64:
  ppc64le:

systems:
  - system-1
  - system-2
  - system-3
""")

S3_CONFIG = yaml.safe_load("""
---
aws:
  prefix: aws-
buckets:
  - description: bucket 1
    name: BUCKET_1
  - description: bucket 2
    name: BUCKET_2
""")

SENTRY_INSTANCES = yaml.safe_load("""
- url: https://sentry.io
  token_name: SENTRY_IO_TOKEN
  org: red-hat
  team: cki
""")


class TestGeneral(unittest.TestCase):
    """Tests not grouped in other classes."""

    def test_all_metrics(self):
        """Test ALL_METRICS has all the expected classes."""
        all_metrics = [
            metrics.AwsMetricsDaily,
            metrics.AwsMetricsMinutely,
            metrics.BeakerMetrics,
            metrics.GitLabMetricsHourly,
            metrics.GitLabMetricsMinutely,
            metrics.QualysMetrics,
            metrics.S3BucketMetrics,
            metrics.TEIIDBeakerMetrics,
            metrics.SentryMetrics,
            metrics.KubernetesMetrics,
            metrics.VolumeMetrics,
        ]
        self.assertEqual(
            len(metrics.ALL_METRICS), len(all_metrics)
        )

        # Check all metrics from all_metrics are in metrics.ALL_METRICS
        for metric in all_metrics:
            self.assertTrue(
                any(m == metric for m in metrics.ALL_METRICS)
            )

    def test_get_enabled_metrics(self):
        """Test get_enabled_metrics detects the enabled metrics correctly."""
        service_metrics.METRICS_CONFIG = {}
        self.assertEqual([], service_metrics.get_enabled_metrics())

        service_metrics.METRICS_CONFIG = {'volumemetrics_enabled': True}
        self.assertEqual(
            set([metrics.VolumeMetrics]),
            set(m.__class__ for m in service_metrics.get_enabled_metrics()),
        )

        service_metrics.METRICS_CONFIG = {
            'volumemetrics_enabled': True,
            's3bucketmetrics_enabled': True,
            'sentrymetrics_enabled': False,
        }
        self.assertEqual(
            set([metrics.VolumeMetrics, metrics.S3BucketMetrics]),
            set(m.__class__ for m in service_metrics.get_enabled_metrics()),
        )

    def test_get_enabled_metrics_default_enabled(self):
        """Test get_enabled_metrics with default_enabled."""
        service_metrics.METRICS_CONFIG = {'default_enabled': False}
        self.assertEqual([], service_metrics.get_enabled_metrics())

        service_metrics.METRICS_CONFIG = {'default_enabled': True}
        self.assertEqual(
            set(metrics.ALL_METRICS),
            set(m.__class__ for m in service_metrics.get_enabled_metrics()),
        )

        service_metrics.METRICS_CONFIG = {
            'default_enabled': True,
            'kubernetesmetrics_enabled': False,
            'volumemetrics_enabled': False,
        }
        self.assertEqual(
            set(metrics.ALL_METRICS) - {metrics.KubernetesMetrics, metrics.VolumeMetrics},
            set(m.__class__ for m in service_metrics.get_enabled_metrics()),
        )

        service_metrics.METRICS_CONFIG = {
            'default_enabled': False,
            'kubernetesmetrics_enabled': True,
            'volumemetrics_enabled': True,
        }
        self.assertEqual(
            {metrics.KubernetesMetrics, metrics.VolumeMetrics},
            set(m.__class__ for m in service_metrics.get_enabled_metrics()),
        )

    def test_crontab(self):
        """Test crontab processing."""
        metric = CronJob()
        metric.run = mock.Mock()
        metric.schedule = '0 1,2 * * *'
        with freeze_time('2000-01-01T00:00:00', tz_offset=-2) as frozen_time:
            # initial run
            metric.check_and_run()
            self.assertEqual(metric.run.mock_calls, [mock.call(last_run_datetime=None)])
            metric.run.mock_calls = []

            # not run again if initial run happened
            metric.check_and_run()
            self.assertEqual(metric.run.mock_calls, [])

            # not run again if schedule did not expire
            frozen_time.tick(delta=datetime.timedelta(minutes=30))
            metric.check_and_run()
            self.assertEqual(metric.run.mock_calls, [])

            # run if schedule expires
            frozen_time.tick(delta=datetime.timedelta(minutes=60))
            metric.check_and_run()
            self.assertEqual(metric.run.mock_calls, [
                mock.call(last_run_datetime=datetime.datetime.fromisoformat(
                    '2000-01-01T00:00:00').replace(tzinfo=datetime.timezone.utc)),
            ])
            metric.run.mock_calls = []

            # run again if schedule expires again
            frozen_time.tick(delta=datetime.timedelta(minutes=60))
            metric.check_and_run()
            self.assertEqual(metric.run.mock_calls, [
                mock.call(last_run_datetime=datetime.datetime.fromisoformat(
                    '2000-01-01T01:30:00').replace(tzinfo=datetime.timezone.utc)),
            ])
            metric.run.mock_calls = []


class TestBeakerMetrics(unittest.TestCase):
    """Test BeakerMetrics methods."""

    @staticmethod
    @mock.patch('cki.cki_tools.service_metrics.metrics.beaker.BEAKER_CONFIG', BEAKER_CONFIG)
    @responses.activate
    def test_update_pool_count():
        """Test update_pool_count method."""
        responses.add(
            responses.GET, 'https://beaker.com/pools/pool-1',
            json={'systems': ['system-1', 'system-2', 'system-3']}
        )
        responses.add(
            responses.GET, 'https://beaker.com/pools/pool-2',
            json={'systems': ['system-4']}
        )
        beaker_metrics = metrics.BeakerMetrics()

        beaker_metrics.metric_pool_count = mock.Mock()
        beaker_metrics.update_pool_count()

        beaker_metrics.metric_pool_count.assert_has_calls([
            mock.call.labels('pool-1'),
            mock.call.labels().set(3),
            mock.call.labels('pool-2'),
            mock.call.labels().set(1),
        ])

    @staticmethod
    @mock.patch('cki.cki_tools.service_metrics.metrics.beaker.BEAKER_CONFIG', BEAKER_CONFIG)
    @responses.activate
    def test_update_system():
        """Test update_system method."""
        responses.add(
            responses.GET, 'https://beaker.com/systems/system-1',
            json={
                'status': 'Automated',
                'type': 'Machine',
                'can_reserve': True,
                'current_reservation': {
                    'recipe_id': 1234,
                    'user': {'user_name': 'maybe-not-cki'}
                },
                'current_loan': {'recipient': 'some-team'},
            }
        )
        responses.add(
            responses.GET, 'https://beaker.com/systems/system-2',
            json={
                'status': 'Automated',
                'type': 'Machine',
                'can_reserve': True,
                'current_reservation': None,
            }
        )
        responses.add(
            responses.GET, 'https://beaker.com/systems/system-3',
            status=404
        )
        beaker_metrics = metrics.BeakerMetrics()

        beaker_metrics.metric_system = mock.Mock()
        beaker_metrics.update_system()

        beaker_metrics.metric_system.assert_has_calls([
            mock.call.labels('system-1'),
            mock.call.labels().info({
                'status': 'Automated',
                'type': 'Machine',
                'can_reserve': 'true',
                'recipe_id': '1234',
                'loaned_to': 'some-team',
                'reserved_by': 'maybe-not-cki',
            }),
            mock.call.labels('system-2'),
            mock.call.labels().info({
                'status': 'Automated',
                'type': 'Machine',
                'can_reserve': 'true',
                'recipe_id': '0',
                'loaned_to': '',
                'reserved_by': '',
            }),
            mock.call.labels('system-3'),
            mock.call.labels().info({
                'status': 'Invalid',
                'type': 'Machine',
                'can_reserve': 'true',
                'recipe_id': '0',
                'loaned_to': '',
                'reserved_by': '',
            }),
        ])

    def test_update(self):
        """Test update calls all the methods."""
        beaker_metrics = metrics.BeakerMetrics()

        beaker_metrics.login = mock.Mock()
        beaker_metrics.update_system = mock.Mock()
        beaker_metrics.update_pool_count = mock.Mock()
        beaker_metrics.run()

        self.assertTrue(beaker_metrics.update_system.called)
        self.assertTrue(beaker_metrics.update_pool_count.called)


class TestS3BucketMetrics(unittest.TestCase):
    """Test S3BucketMetrics methods."""

    @staticmethod
    @mock.patch.dict(
        os.environ, {
            'BUCKET_1': 'endpoint|access_key|secret_key|bucket|path',
            'BUCKET_2': 'endpoint|access_key|secret_key|bucket|path',
        }
    )
    @mock.patch('cki.cki_tools.service_metrics.metrics.s3buckets.S3_CONFIG', S3_CONFIG)
    @mock.patch('cki.cki_tools.service_metrics.metrics.s3buckets.get_bucket_size')
    @mock.patch('boto3.Session')
    @responses.activate
    def test_update(session, mock_get_bucket_size):
        """Test update method."""
        session().client().list_buckets.return_value = {'Buckets': [
            {'Name': 'aws-foo'}, {'Name': 'bar-baz'}
        ]}
        session().client().get_metric_data.return_value = {'MetricDataResults': [
            {'Id': 'bucketsizebytes_0', 'Values': [1]},
            {'Id': 'numberofobjects_0', 'Values': []},
        ]}

        s3_metrics = metrics.S3BucketMetrics()

        mock_get_bucket_size.return_value = (1024, 2)
        s3_metrics.metric_bytes = mock.Mock()
        s3_metrics.metric_file_count = mock.Mock()

        s3_metrics.run()

        s3_metrics.metric_bytes.assert_has_calls([
            mock.call.labels('BUCKET_1', 'bucket 1'),
            mock.call.labels().set(1024),
            mock.call.labels('BUCKET_2', 'bucket 2'),
            mock.call.labels().set(1024),
            mock.call.labels('aws-foo', 'aws-foo'),
            mock.call.labels().set(1),
        ])
        s3_metrics.metric_file_count.assert_has_calls([
            mock.call.labels('BUCKET_1', 'bucket 1'),
            mock.call.labels().set(2),
            mock.call.labels('BUCKET_2', 'bucket 2'),
            mock.call.labels().set(2),
            mock.call.labels('aws-foo', 'aws-foo'),
            mock.call.labels().set(0),
        ])


class TestTEIIDBeakerMetrics(unittest.TestCase):
    """Test TEIIDBeakerMetrics methods."""

    def test_recipe_matches_queue(self):
        # pylint: disable=protected-access
        """Test _recipe_matches_queue method."""
        tbm = metrics.TEIIDBeakerMetrics()

        recipe_distro_required = """
            <distroRequires>
             <distro_arch op="=" value="i386"/>
             <distro_arch op="=" value="x86_64"/>
             <distro_family op="=" value="Fedora34"/>
             <distro_variant op="=" value="Server"/>
             <distro_name op="=" value="Fedora-34"/>
            </distroRequires>
        """
        self.assertFalse(tbm._recipe_matches_queue(recipe_distro_required, ['x86_64']))
        self.assertTrue(tbm._recipe_matches_queue(recipe_distro_required, ['x86_64', 'i386']))
        self.assertFalse(tbm._recipe_matches_queue(recipe_distro_required, ['i386']))
        self.assertFalse(tbm._recipe_matches_queue(recipe_distro_required, ['i386', 'aarch64']))

    @staticmethod
    @mock.patch('cki.cki_tools.service_metrics.metrics.teiidbeaker.BEAKER_CONFIG', BEAKER_CONFIG)
    @responses.activate
    def test_update_cki_beaker_recipes_in_queue():
        """Test update_cki_beaker_recipes_in_queue method."""
        tbm = metrics.TEIIDBeakerMetrics()
        tbm.metric_recipes_in_queue = mock.Mock()
        tbm.get_queued_recipes_constraints = mock.Mock()
        tbm.get_queued_recipes_constraints.return_value = [
            """<distroRequires>
             <distro_arch op="=" value="i386"/>
             <distro_arch op="=" value="x86_64"/>
            </distroRequires>""",
            """<distroRequires>
             <distro_arch op="=" value="x86_64"/>
            </distroRequires>""",
            """<distroRequires>
             <distro_arch op="=" value="aarch64"/>
            </distroRequires>""",
            """<distroRequires>
             <distro_arch op="=" value="x86_64"/>
            </distroRequires>""",
            """<distroRequires>
             <distro_arch op="=" value="ppc64"/>
            </distroRequires>""",
            """<distroRequires>
             <distro_arch op="=" value="ppc64le"/>
            </distroRequires>""",
        ]

        tbm.update_cki_beaker_recipes_in_queue()

        tbm.metric_recipes_in_queue.assert_has_calls([
            mock.call.labels('aarch64'),
            mock.call.labels().set(1),
            mock.call.labels('x86_64'),
            mock.call.labels().set(2),
            mock.call.labels('i386'),
            mock.call.labels().set(1),
            mock.call.labels('ppc64'),
            mock.call.labels().set(1),
            mock.call.labels('ppc64le'),
            mock.call.labels().set(1),
        ])

    @staticmethod
    @mock.patch('cki.cki_tools.service_metrics.metrics.teiidbeaker.ALL_STATUSES',
                {'New', 'Installing', 'Aborted'})
    @mock.patch('cki.cki_tools.service_metrics.metrics.teiidbeaker.ALL_PRIORITIES',
                {'Normal', 'High'})
    def test_update_cki_beaker_recipes_by_status():
        """Test update_cki_beaker_recipes_by_status method."""
        tbm = metrics.TEIIDBeakerMetrics()
        tbm.metric_recipes_by_status = mock.Mock()
        tbm.get_recipes_by_status = mock.Mock()
        tbm.get_recipes_by_status.return_value = [
            ("New", "High", 116),
            ("New", "Normal", 16),
            ("Installing", "Normal", 8),
        ]

        tbm.update_cki_beaker_recipes_by_status()

        tbm.metric_recipes_by_status.assert_has_calls([
            mock.call.labels('New', 'High'),
            mock.call.labels().set(116),
            mock.call.labels('New', 'Normal'),
            mock.call.labels().set(16),
            mock.call.labels('Installing', 'Normal'),
            mock.call.labels().set(8),
            # Now the ones set to zero.
            mock.call.labels('Aborted', 'High'),
            mock.call.labels().set(0),
            mock.call.labels('Aborted', 'Normal'),
            mock.call.labels().set(0),
            mock.call.labels('Installing', 'High'),
            mock.call.labels().set(0)
        ])

    def test_update(self):
        """Test update calls all the methods."""
        tbm = metrics.TEIIDBeakerMetrics()

        tbm.update_cki_beaker_recipes_in_queue = mock.Mock()
        tbm.update_cki_beaker_recipes_by_status = mock.Mock()
        tbm.update_cki_beaker_recipes_queue_time = mock.Mock()

        tbm.run()

        self.assertTrue(tbm.update_cki_beaker_recipes_in_queue.called)
        self.assertTrue(tbm.update_cki_beaker_recipes_by_status.called)
        self.assertTrue(tbm.update_cki_beaker_recipes_queue_time.called)

    @staticmethod
    def test_update_cki_beaker_recipes_queue_time():
        """Test update_cki_beaker_recipes_queue_time method."""
        tbm = metrics.TEIIDBeakerMetrics()
        tbm.metric_recipes_queue_time = mock.Mock()
        tbm.db_handler.execute = mock.Mock()

        now = datetime.datetime(2022, 8, 17, 14, 20, 5, 573000)
        tbm.db_handler.execute.return_value = [
            [datetime.datetime(2022, 7, 20, 10, 57, 51), now],
            [datetime.datetime(2022, 7, 20, 11, 6, 31), now],
            [datetime.datetime(2022, 7, 20, 11, 7, 35), now],
        ]

        tbm.update_cki_beaker_recipes_queue_time()

        tbm.metric_recipes_queue_time.assert_has_calls([
            mock.call.observe(2431334.573),
            mock.call.observe(2430814.573),
            mock.call.observe(2430750.573)
        ])


@mock.patch('cki.cki_tools.service_metrics.metrics.sentry.SENTRY_INSTANCES', SENTRY_INSTANCES)
@mock.patch.dict(os.environ, SENTRY_IO_TOKEN='some-token')
class TestSentryMetrics(unittest.TestCase):
    """Test SentryMetrics methods."""

    @responses.activate
    def test_update_project(self):
        """Test update_project method."""
        responses.add(
            responses.GET, 'https://sentry.io/api/0/teams/red-hat/cki/projects/',
            json=[{'slug': 'project'}]
        )
        responses.add(
            responses.GET, 'https://sentry.io/api/0/projects/red-hat/project/stats/?resolution=1d',
            json=[[1.0, 123], ]
        )

        project = {'slug': 'project'}
        instance = SENTRY_INSTANCES[0]

        sentry_metrics = metrics.SentryMetrics()
        sentry_metrics.metric_sentry_events = mock.Mock()

        sentry_metrics.update_project(instance, project)

        sentry_metrics.metric_sentry_events.assert_has_calls([
            mock.call.labels('https://sentry.io', 'red-hat', 'project'),
            mock.call.labels().set(123)
        ])

    def test_update(self):
        """Test update call."""
        sentry_metrics = metrics.SentryMetrics()
        # pylint: disable=protected-access
        sentry_metrics._apis['https://sentry.io'].projects = [
            {'slug': 'proj-1'}, {'slug': 'proj-2'}, {'slug': 'proj-3'},
        ]
        sentry_metrics.update_project = mock.Mock()

        sentry_metrics.run()
        sentry_metrics.update_project.assert_has_calls([
            mock.call(SENTRY_INSTANCES[0], {'slug': 'proj-1'}),
            mock.call(SENTRY_INSTANCES[0], {'slug': 'proj-2'}),
            mock.call(SENTRY_INSTANCES[0], {'slug': 'proj-3'}),
        ])


@mock.patch.dict(os.environ, SENTRY_IO_TOKEN='some-token')
class TestSentryAPI(unittest.TestCase):
    """Test SentryAPI methods."""

    @responses.activate
    def test_get(self):
        """Test _get method."""
        responses.add(
            responses.GET, 'https://sentry.io/foo/red-hat/cki', json={}
        )
        api = sentry.SentryAPI(SENTRY_INSTANCES[0])
        api._get('/foo/{org}/{team}')  # pylint: disable=protected-access

        self.assertEqual(
            responses.calls[0].request.url,
            'https://sentry.io/foo/red-hat/cki'
        )
        self.assertEqual(
            responses.calls[0].request.headers['Authorization'],
            'Bearer some-token'
        )

    def test_projects(self):
        # pylint: disable=protected-access
        """Test projects property."""
        api = sentry.SentryAPI(SENTRY_INSTANCES[0])
        api._get = mock.Mock()

        api.projects  # pylint: disable=pointless-statement

        api._get.assert_called_with('/api/0/teams/{org}/{team}/projects/')

    def test_project_stats(self):
        # pylint: disable=protected-access
        """Test get_project_stats method."""
        api = sentry.SentryAPI(SENTRY_INSTANCES[0])
        api._get = mock.Mock()

        proj = {'slug': 'proj-1', 'foo': 'bar'}
        api.get_project_stats(proj)

        api._get.assert_called_with('/api/0/projects/{org}/proj-1/stats/?resolution=1d')


@mock.patch('cki_lib.kubernetes.KubernetesHelper.setup', mock.Mock())
class TestKubernetesMetrics(unittest.TestCase):
    """Test KubernetesMetrics methods."""

    @URLLIB3_MOCK.activate
    def test_update_cluster(self):
        # pylint: disable=protected-access
        """Test update_cluster method."""
        body = {
            'items': [
                {'metadata': {'name': 'some-pod-55-8fgnw'},
                 'containers': [
                    {'name': 'promtail-sidecar', 'usage': {'cpu': '5m', 'memory': '46696Ki'}},
                    {'name': 'default', 'usage': {'cpu': '2m', 'memory': '70392Ki'}}
                ]},
                {'metadata': {'name': 'another-pod-5-aaaaa'},
                 'containers': [
                    {'name': 'promtail-sidecar', 'usage': {'cpu': '1025m', 'memory': '4Mi'}},
                    {'name': 'default', 'usage': {'cpu': '0', 'memory': '0'}}
                ]},
            ]
        }

        URLLIB3_MOCK.add(
            'GET', '/apis/metrics.k8s.io/v1beta1/namespaces/cki/pods',
            body=json.dumps(body), status=200, content_type='application/json'
        )

        k8s_metrics = metrics.KubernetesMetrics()
        k8s_metrics.k8s.namespace = 'cki'
        k8s_metrics.metric_usage = mock.Mock()
        k8s_metrics._delete_missing_containers = mock.Mock()

        k8s_metrics.update_cluster()

        k8s_metrics.metric_usage.assert_has_calls([
            mock.call.labels('some-pod-55-8fgnw', 'promtail-sidecar', 'memory'),
            mock.call.labels().set(47816704.0),
            mock.call.labels('some-pod-55-8fgnw', 'promtail-sidecar', 'cpu'),
            mock.call.labels().set(0.005),
            mock.call.labels('some-pod-55-8fgnw', 'default', 'memory'),
            mock.call.labels().set(72081408.0),
            mock.call.labels('some-pod-55-8fgnw', 'default', 'cpu'),
            mock.call.labels().set(0.002),
            mock.call.labels('another-pod-5-aaaaa', 'promtail-sidecar', 'memory'),
            mock.call.labels().set(4194304.0),
            mock.call.labels('another-pod-5-aaaaa', 'promtail-sidecar', 'cpu'),
            mock.call.labels().set(1.025),
            mock.call.labels('another-pod-5-aaaaa', 'default', 'memory'),
            mock.call.labels().set(0.0),
            mock.call.labels('another-pod-5-aaaaa', 'default', 'cpu'),
            mock.call.labels().set(0.0),
        ])
        k8s_metrics._delete_missing_containers.assert_called_with(body['items'])

    def test_update(self):
        """Test update calls update_cluster correctly."""
        k8s_metrics = metrics.KubernetesMetrics()
        k8s_metrics.update_cluster = mock.Mock()

        k8s_metrics.run()

        self.assertTrue(k8s_metrics.update_cluster.called)

    @URLLIB3_MOCK.activate
    def test_delete_missing_containers(self):
        # pylint: disable=protected-access
        """Test _delete_missing_containers method."""
        body = {
            'items': [
                {'metadata': {'name': 'some-pod-55-8fgnw'},
                 'containers': [
                    {'name': 'promtail-sidecar', 'usage': {'cpu': '5m', 'memory': '46696Ki'}},
                    {'name': 'default', 'usage': {'cpu': '2m', 'memory': '70392Ki'}}
                ]},
                {'metadata': {'name': 'another-pod-5-aaaaa'},
                 'containers': [
                    {'name': 'promtail-sidecar', 'usage': {'cpu': '1025m', 'memory': '4Mi'}},
                    {'name': 'default', 'usage': {'cpu': '0', 'memory': '0'}}
                ]},
            ]
        }

        k8s_metrics = metrics.KubernetesMetrics()
        k8s_metrics.k8s.namespace = 'cki'
        k8s_metrics.metric_usage.remove = mock.Mock()

        # Update with all the pods
        URLLIB3_MOCK.add(
            'GET', '/apis/metrics.k8s.io/v1beta1/namespaces/cki/pods',
            body=json.dumps(body), status=200, content_type='application/json'
        )
        k8s_metrics.update_cluster()

        # Delete one pod from the response and call update again
        del body['items'][1]
        URLLIB3_MOCK.reset()
        URLLIB3_MOCK.add(
            'GET', '/apis/metrics.k8s.io/v1beta1/namespaces/cki/pods',
            body=json.dumps(body), status=200, content_type='application/json'
        )
        k8s_metrics.update_cluster()

        # Missing pod is deleted from labels
        k8s_metrics.metric_usage.remove.assert_has_calls([
            mock.call('another-pod-5-aaaaa', 'default', 'memory'),
            mock.call('another-pod-5-aaaaa', 'default', 'cpu'),
            mock.call('another-pod-5-aaaaa', 'promtail-sidecar', 'memory'),
            mock.call('another-pod-5-aaaaa', 'promtail-sidecar', 'cpu')
        ], any_order=True)


class TestVolumeMetrics(unittest.TestCase):
    """Test VolumeMetrics methods."""

    @staticmethod
    @mock.patch('cki.cki_tools.service_metrics.metrics.volume.shutil.disk_usage')
    @mock.patch('cki.cki_tools.service_metrics.metrics.volume.VOLUMES_LIST',
                [{'name': 'vol_1', 'path': '/path_1'}, {'name': 'vol_2', 'path': '/path_2'}])
    def test_update(mock_disk_usage):
        """Test update_pool_count method."""
        usage = mock.Mock()
        usage.used = 123
        mock_disk_usage.return_value = usage

        volume_metrics = metrics.VolumeMetrics()
        volume_metrics.metric_usage = mock.Mock()

        volume_metrics.run()

        mock_disk_usage.assert_has_calls([
            mock.call('/path_1'),
            mock.call('/path_2'),
        ])

        volume_metrics.metric_usage.assert_has_calls([
            mock.call.labels('vol_1'),
            mock.call.labels().set(123),
            mock.call.labels('vol_2'),
            mock.call.labels().set(123)
        ])


class TestAwsMetrics(unittest.TestCase):
    """Test AwsMetrics methods."""

    def _get_cost_and_usage(self, GroupBy=None, *args, **kwargs):
        if GroupBy[0]['Key'] == 'ServiceComponent':
            return {
                'ResultsByTime': [{
                    'TimePeriod': {'Start': '2021-07-15', 'End': '2021-07-16'},
                    'Groups': [{
                        'Keys': ['ServiceComponent$Build'],
                        'Metrics': {'BlendedCost': {'Amount': '1', 'Unit': 'USD'}}
                    }, {
                        'Keys': ['ServiceComponent$DomainNames'],
                        'Metrics': {'BlendedCost': {'Amount': '2', 'Unit': 'USD'}}
                    }]
                }]}
        if GroupBy[0]['Key'] == 'ServiceOwner':
            return {
                'ResultsByTime': [{
                    'TimePeriod': {'Start': '2021-07-15', 'End': '2021-07-16'},
                    'Groups': [{
                        'Keys': ['ServiceOwner$CkiProject'],
                        'Metrics': {'BlendedCost': {'Amount': '3', 'Unit': 'USD'}}
                    }]
                }]}
        if GroupBy[0]['Key'] == 'SERVICE':
            return {
                'ResultsByTime': [{
                    'TimePeriod': {'Start': '2021-07-15', 'End': '2021-07-16'},
                    'Groups': [{
                        'Keys': ['AWS Lambda'],
                        'Metrics': {'BlendedCost': {'Amount': '4', 'Unit': 'USD'}}
                    }, {
                        'Keys': ['EC2 - Other'],
                        'Metrics': {'BlendedCost': {'Amount': '5', 'Unit': 'USD'}}
                    }]
                }]}
        if GroupBy[0]['Key'] == 'USAGE_TYPE':
            return {'ResultsByTime': [{
                'TimePeriod': {'Start': '2021-07-15', 'End': '2021-07-16'},
                'Groups': [{
                    'Keys': ['BoxUsage:t3a.medium'],
                    'Metrics': {'BlendedCost': {'Amount': '6', 'Unit': 'USD'}}
                }, {
                    'Keys': ['BoxUsage:t3a.micro'],
                    'Metrics': {'BlendedCost': {'Amount': '7', 'Unit': 'USD'}}
                }]
            }]}
        if GroupBy[0]['Key'] == 'INSTANCE_TYPE':
            return {'ResultsByTime': [{
                'TimePeriod': {'Start': '2021-07-15', 'End': '2021-07-16'},
                'Groups': [{
                    'Keys': ['NoInstanceType'],
                    'Metrics': {'BlendedCost': {'Amount': '8', 'Unit': 'USD'}}
                }, {
                    'Keys': ['c5d.4xlarge'],
                    'Metrics': {'BlendedCost': {'Amount': '9', 'Unit': 'USD'}}
                }]
            }]}
        if GroupBy[0]['Key'] == 'OPERATION':
            return {'ResultsByTime': [{
                'TimePeriod': {'Start': '2021-07-15', 'End': '2021-07-16'},
                'Groups': [{
                    'Keys': ['A'],
                    'Metrics': {'BlendedCost': {'Amount': '10', 'Unit': 'USD'}}
                }, {
                    'Keys': ['AAAA'],
                    'Metrics': {'BlendedCost': {'Amount': '11', 'Unit': 'USD'}}
                }]
            }]}

    @mock.patch('boto3.Session')
    def test_costs(self, session):
        """Test aws cost metrics."""
        aws_metrics = metrics.AwsMetricsDaily()
        aws_metrics.metric_cost = mock.Mock()

        session().client().get_cost_and_usage.side_effect = self._get_cost_and_usage
        aws_metrics.run()

        aws_metrics.metric_cost.assert_has_calls([
            mock.call.labels('all', 'component', 'Build').set(1.0),
            mock.call.labels('all', 'component', 'DomainNames').set(2.0),
            mock.call.labels('all', 'owner', 'CkiProject').set(3.0),
            mock.call.labels('all', 'service', 'AWS Lambda').set(4.0),
            mock.call.labels('all', 'service', 'EC2-Other').set(5.0),
            mock.call.labels('all', 'usage_type', 't3a.medium').set(6.0),
            mock.call.labels('all', 'usage_type', 't3a.micro').set(7.0),
            mock.call.labels('all', 'instance_type', 'NoInstanceType').set(8.0),
            mock.call.labels('all', 'instance_type', 'c5d.4xlarge').set(9.0),
            mock.call.labels('all', 'operation', 'A').set(10.0),
            mock.call.labels('all', 'operation', 'AAAA').set(11.0),
        ], any_order=True)

    @mock.patch('boto3.Session')
    def test_reserved_instances(self, session):
        """Test aws reserved instance metrics."""
        aws_metrics = metrics.AwsMetricsDaily()
        aws_metrics.metric_reserved_instance_end = mock.Mock()
        session().client().describe_reserved_instances.return_value = {'ReservedInstances': [{
            'End': datetime.datetime(2023, 3, 23, 9, 18, 37, tzinfo=datetime.timezone.utc),
            'InstanceCount': 6,
            'InstanceType': 'r5.large',
            'ReservedInstancesId': 'dead-beaf',
            'State': 'active',
        }]}
        aws_metrics.run()

        aws_metrics.metric_reserved_instance_end.assert_has_calls([
            mock.call.labels('dead-beaf', 6, 'r5.large', 'active').set(1679563117.0),
        ])

    @mock.patch('boto3.Session')
    def test_subnets(self, session):
        """Test aws subnets metrics."""
        aws_metrics = metrics.AwsMetricsMinutely()
        aws_metrics.metric_subnets = mock.Mock()
        session().client().describe_subnets.return_value = {'Subnets': [{
            'AvailabilityZone': 'us-east-1b',
            'AvailableIpAddressCount': 102,
            'CidrBlock': '1.2.3.4/5',
            'SubnetId': 'subnet-1',
            'VpcId': 'vpc-1',
            'Tags': [{'Key': 'Name', 'Value': 'subnetname'}],
        }, {
            'AvailabilityZone': 'us-east-1c',
            'AvailableIpAddressCount': 103,
            'CidrBlock': '1.2.3.4/6',
            'SubnetId': 'subnet-2',
            'VpcId': 'vpc-2',
            'Tags': [],
        }, {
            'AvailabilityZone': 'us-east-1d',
            'AvailableIpAddressCount': 104,
            'CidrBlock': '1.2.3.4/7',
            'SubnetId': 'subnet-3',
            'VpcId': 'vpc-3',
        }]}
        aws_metrics.run()

        aws_metrics.metric_subnets.assert_has_calls([
            mock.call.labels('vpc-1', 'subnet-1', 'us-east-1b', '1.2.3.4/5', 'subnetname'),
            mock.call.labels().set(102),
            mock.call.labels('vpc-2', 'subnet-2', 'us-east-1c', '1.2.3.4/6', ''),
            mock.call.labels().set(103),
            mock.call.labels('vpc-3', 'subnet-3', 'us-east-1d', '1.2.3.4/7', ''),
            mock.call.labels().set(104),
        ])


class TestGitLabMetrics(unittest.TestCase):
    """Test GitLabMetrics methods."""

    @staticmethod
    @mock.patch('cki.cki_tools.service_metrics.metrics.gitlab.GITLAB_CONFIG',
                {'namespaces': ['https://instance/foo', 'https://instance/bar']})
    @responses.activate
    def test_namespace_plans():
        """Test update_pool_count method."""
        responses.add(responses.GET, 'https://instance/api/v4/namespaces/foo',
                      json={'full_path': 'foo', 'plan': 'free'})
        responses.add(responses.GET, 'https://instance/api/v4/namespaces/bar',
                      json={'full_path': 'bar', 'plan': 'baz'})
        responses.add(
            responses.POST, 'https://instance/api/graphql',
            json={'data': {'namespace': {
                'fullPath': 'foo',
                'rootStorageStatistics': {'repositorySize': 1},
                'projects': {'nodes': []},
            }}}
        )
        responses.add(
            responses.POST, 'https://instance/api/graphql',
            json={'data': {'namespace': {
                'fullPath': 'bar',
                'rootStorageStatistics': None,
                'projects': {'nodes': [{
                    'fullPath': 'baz',
                    'archived': False,
                    'statistics': {'storageSize': 2}
                }]},
            }}}
        )
        gitlab_metrics = metrics.GitLabMetricsHourly()

        gitlab_metrics.metric_namespace_plan = mock.Mock()
        gitlab_metrics.metric_namespace_size = mock.Mock()
        gitlab_metrics.metric_project_size = mock.Mock()
        gitlab_metrics.run()

        gitlab_metrics.metric_namespace_plan.assert_has_calls([
            mock.call.labels('https://instance', 'foo').state('free'),
            mock.call.labels('https://instance', 'bar').state('unknown'),
        ], any_order=True)
        gitlab_metrics.metric_namespace_size.assert_has_calls([
            mock.call.labels('https://instance', 'foo', 'repositorySize').set(1),
        ], any_order=True)
        gitlab_metrics.metric_project_size.assert_has_calls([
            mock.call.labels('https://instance', 'baz', 'false', 'storageSize').set(2),
        ], any_order=True)

    @mock.patch('cki.cki_tools.service_metrics.metrics.gitlab.GITLAB_CONFIG',
                {'projects': ['https://instance/foo/bar/baz/qux']})
    @responses.activate
    def test_webhook_alert_status(self) -> None:
        """Test webhook alerts."""
        responses.add(responses.GET, 'https://instance/api/v4/groups/foo/hooks',
                      status=403)
        responses.add(responses.GET, 'https://instance/api/v4/groups/foo%2Fbar/hooks',
                      json=[{'alert_status': 'executable', 'url': 'url2'}])
        responses.add(responses.GET, 'https://instance/api/v4/groups/foo%2Fbar%2Fbaz/hooks',
                      json=[{'alert_status': 'disabled', 'url': 'url3'}])
        responses.add(responses.GET, 'https://instance/api/v4/projects/foo%2Fbar%2Fbaz%2Fqux/hooks',
                      json=[{'alert_status': 'something', 'url': 'url4'}])

        gitlab_metrics = metrics.GitLabMetricsMinutely()

        gitlab_metrics.metric_webhook_alert_status = mock.Mock()
        gitlab_metrics.update_metric_webhook_alert_status()

        self.assertCountEqual(gitlab_metrics.metric_webhook_alert_status.mock_calls, [
            mock.call.labels('https://instance', 'foo/bar', 'url2'),
            mock.call.labels().state('executable'),
            mock.call.labels('https://instance', 'foo/bar/baz', 'url3'),
            mock.call.labels().state('disabled'),
            mock.call.labels('https://instance', 'foo/bar/baz/qux', 'url4'),
            mock.call.labels().state('unknown'),
        ])


class TestQualysMetrics(unittest.TestCase):
    """Test QualysMetrics methods."""

    @staticmethod
    @mock.patch('cki.cki_tools.service_metrics.metrics.qualys.QUALYS_CONFIG',
                {'pop3': {'user': 'username', 'pass': 'pass-env', 'host': 'hostname', 'port': 1234},
                 'bucket': 'bucket-env'})
    @mock.patch.dict(os.environ, {'pass-env': 'pass-value', 'bucket-env': '|||bucket|prefix/'})
    @mock.patch('poplib.POP3_SSL', autospec=True)
    @mock.patch('boto3.Session')
    def test_s3_upload(session, pop):
        """Test update_pool_count method."""
        pop.return_value.list.return_value = None, [b'1 1', b'2 2'], None
        pop.return_value.top.side_effect = [
            [None, [
                b'Date: Sun, 09 Oct 2022 00:00:00 +0400',
            ]],
            [None, [
                b'Date: Sun, 09 Oct 2022 00:00:00 +0400',
                b'Content-Type: application/octet-stream',
                b'Content-Disposition: attachment; filename="name.csv"',
                b'',
                b'weirdness',
                b'IP,some,field',
                b'data,data,data',
            ]],
        ]
        qualys_metrics = metrics.QualysMetrics()
        qualys_metrics.run()
        pop.assert_called_with('hostname', 1234)
        pop.return_value.user.assert_called_once_with('username')
        pop.return_value.pass_.assert_called_once_with('pass-value')
        pop.return_value.list.assert_called()
        pop.return_value.top.assert_has_calls([mock.call(1, 1), mock.call(2, 2)])
        pop.return_value.dele.assert_has_calls([mock.call(1), mock.call(2)])
        pop.return_value.quit.assert_called()
        session().client().put_object.assert_has_calls([
            mock.call(Bucket='bucket', Key='prefix/raw/2022-10-08.csv',
                      Body=b'weirdness\nIP,some,field\ndata,data,data'),
            mock.call(Bucket='bucket', Key='prefix/data.csv',
                      Body=b'IP,some,field\r\ndata,data,data\r\n'),
        ])
