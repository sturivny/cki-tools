"""Tests for the cki.deployment_tools.secrets module."""
import contextlib
import io
import os
import pathlib
import subprocess
import tempfile
import typing
import unittest
from unittest import mock

import responses
import yaml

from cki.deployment_tools import secrets


@unittest.skipUnless(b'pbkdf' in subprocess.run(
    ['openssl', 'enc', '-help'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=False
).stdout, 'OpenSSL version too old')
class TestSecrets(unittest.TestCase):
    """Test yaml utils."""

    @staticmethod
    @contextlib.contextmanager
    def _setup_secrets(
        variables: typing.Dict[str, typing.Any],
    ) -> typing.Iterator[str]:
        with tempfile.TemporaryDirectory() as directory:
            for name, values in variables.items():
                pathlib.Path(directory, name).write_text(yaml.safe_dump(values), encoding='utf8')
            yield directory

    def test_variables(self) -> None:
        """Check that basic variable processing works."""
        with self._setup_secrets({
                'internal.yml': {'bar': 0, 'foo': 'qux', 'sec': 'none'},
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/internal.yml',
        }):
            self.assertEqual(secrets.variable('bar'), 0)
            self.assertEqual(secrets.variable('foo'), 'qux')

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_variables_encrypted(self) -> None:
        """Check that encrypted variables cannot be read."""
        with self._setup_secrets({
                'internal.yml': {'foo': secrets.encrypt('qux')},
                'secrets.yml': {'sec': secrets.encrypt('ure')},
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/internal.yml',
            'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
        }):
            self.assertRaises(Exception, lambda: secrets.variable('foo'))
            self.assertRaises(Exception, lambda: secrets.variable('sec'))

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_secrets(self) -> None:
        """Check that basic secret processing works."""
        with self._setup_secrets({
                'internal.yml': {'foo': secrets.encrypt('qux')},
                'secrets.yml': {'sec': secrets.encrypt('ure')},
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/internal.yml',
            'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
        }):
            self.assertRaises(Exception, lambda: secrets.secret('foo'))
            self.assertEqual(secrets.secret('sec'), 'ure')

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_secrets_unencrypted(self) -> None:
        """Check that uncrypted secret processing fails."""
        with self._setup_secrets({
                'internal.yml': {'foo': 'qux'},
                'secrets.yml': {'sec': 'ure'},
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/internal.yml',
            'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
        }):
            self.assertRaises(Exception, lambda: secrets.secret('foo'))
            self.assertRaises(Exception, lambda: secrets.secret('sec'))

    def test_secrets_invalid(self) -> None:
        """Check that invalid secret files fail."""
        with self._setup_secrets({
                'internal.yml': ['bar', 'baz'],
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/internal.yml',
        }):
            with self.assertRaises(Exception, msg='invalid'):
                secrets.variable('bar')

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_secrets_custom(self) -> None:
        """Check that custom secrets files work."""
        with self._setup_secrets({
                'custom-secrets.yml': {'bar': secrets.encrypt('baz')},
                'custom-secrets-2.yml': {'foo': secrets.encrypt('sec')},
                'custom-vars.yml': {'ure': 'qux'},
        }) as directory, mock.patch.dict(os.environ, {
            'CUSTOM_SECRETS_FILE': f'{directory}/custom-secrets.yml',
            'CUSTOM_SECRETS_FILE_2': f'{directory}/custom-secrets-2.yml',
            'CUSTOM_VARS_FILE': f'{directory}/custom-vars.yml',
            'CKI_SECRETS_NAMES': 'CUSTOM_SECRETS_FILE CUSTOM_SECRETS_FILE_2',
            'CKI_VARS_NAMES': 'CUSTOM_VARS_FILE CUSTOM_VARS_FILE_2',
        }):
            self.assertEqual(secrets.secret('bar'), 'baz')
            self.assertEqual(secrets.secret('foo'), 'sec')
            self.assertEqual(secrets.variable('ure'), 'qux')

    @responses.activate
    @mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://host'})
    def test_secrets_vault(self) -> None:
        """Check that basic secret processing from HashiCorp Vault works."""
        responses.add(
            responses.GET, 'https://host/v1/apps/data/cki/legacy/foo',
            json={'data': {'data': {'value': 'bar', 'field': 'baz'}}})

        data = (
            ('foo', 'bar'),
            ('legacy/foo:field', 'baz'),
            ('legacy/foo', {'value': 'bar', 'field': 'baz'}),
            ('qux', None),
            ('legacy/foo:qux', None),
            ('legacy/qux', None),
            ('legacy/qux:qux', None),
        )

        for location, expected in data:
            if expected:
                self.assertEqual(secrets.secret(location), expected)
            else:
                self.assertRaises(Exception, secrets.secret, location)

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_encrypt(self) -> None:
        """Check that basic encryption works."""
        self.assertEqual(secrets.encrypt('bar', salt=False), '0eA1mjLawgSmRg9bpsdPDw==')
        self.assertTrue(secrets.encrypt('bar').startswith('U2FsdGVkX1'))

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_cli(self) -> None:
        """Check that CLI processing works."""
        with self._setup_secrets({
                'internal.yml': {'bar': 0, 'foo': 'qux'},
                'secrets.yml': {'sec': secrets.encrypt('ure')},
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/internal.yml',
            'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
        }):

            cases = (
                ('_variable_cli', ['bar'], '0\n'),
                ('_variable_cli', ['bar', '--json'], '0\n'),
                ('_variable_cli', ['foo'], 'qux\n'),
                ('_variable_cli', ['foo', '--json'], '"qux"\n'),
                ('_secret_cli', ['sec'], 'ure\n'),
                ('_main', ['variable', 'bar'], '0\n'),
                ('_main', ['variable', 'bar', '--json'], '0\n'),
                ('_main', ['variable', 'foo'], 'qux\n'),
                ('_main', ['variable', 'foo', '--json'], '"qux"\n'),
                ('_main', ['secret', 'sec'], 'ure\n'),
            )
            for method, args, expected in cases:
                with contextlib.redirect_stdout(output := io.StringIO()):
                    getattr(secrets, method)(args)
                self.assertEqual(output.getvalue(), expected)
