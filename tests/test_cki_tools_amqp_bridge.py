"""Tests for cki_tools.amqp_bridge."""
import unittest
from unittest import mock

import proton.reactor

from tests.utils import tear_down_registry

tear_down_registry()

# pylint: disable=wrong-import-position
from cki.cki_tools import amqp_bridge  # noqa: E402


class FakeEvent():
    """Dummy event for proton."""

    def __init__(self, *args, **kwargs):
        """Initialize."""


class TestOnStart(unittest.TestCase):
    """Tests for amqp_bridge.AMQP10Receiver.on_start()."""

    @mock.patch('proton._reactor.Container.create_receiver')
    @mock.patch('proton._transport.SSLDomain.set_credentials')
    @mock.patch('proton._reactor.Container.connect')
    def test_topics(self, mock_connect, mock_credentials, mock_create):
        """Verify receiver is set up for all passed topics."""
        mock_connect.return_value = 'connection'
        test_topics = ['test_topic1', 'test_topic2']
        event = FakeEvent()
        event.container = proton.reactor.Container()

        config = {
            'name': 'foo',
            'cert_path': 'cert_path',
            'receiver_urls': ['url1', 'url2'],
            'message_topics': test_topics
        }
        receiver = amqp_bridge.AMQP10Receiver(config)
        receiver.on_start(event)

        mock_connect.assert_called()
        mock_credentials.assert_called()
        self.assertEqual(len(test_topics), mock_create.call_count)


class TestMessageSend(unittest.TestCase):
    """Test message_send method."""

    @mock.patch('cki.cki_tools.amqp_bridge.messagequeue.MessageQueue.send_message')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_send(self, send_message):
        """Test send call."""
        amqp_bridge.RABBITMQ_PUBLISH_EXCHANGE = 'some.exchange'
        message = {'foo': 'bar'}
        headers = {'bar': 'foo'}
        amqp_bridge.message_send(message, 'routing_key', headers)

        self.assertTrue(send_message.called)
        send_message.assert_called_with(message, 'routing_key',
                                        exchange='some.exchange', headers=headers)

    @mock.patch('cki.cki_tools.amqp_bridge.messagequeue.MessageQueue.send_message')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_send_non_production(self, send_message):
        """Test send call."""
        message = {'foo': 'bar'}
        headers = {'bar': 'foo'}
        amqp_bridge.message_send(message, 'routing_key', headers)

        self.assertFalse(send_message.called)


class TestAMQP10Receiver(unittest.TestCase):
    """Test AMQP10Receiver."""

    def test_init(self):
        """Test configs are correctly loaded."""
        config = {
            'name': 'foo',
            'cert_path': 'cert_path',
            'receiver_urls': ['url1', 'url2'],
            'message_topics': ['topic1', 'topic2'],
        }

        receiver = amqp_bridge.AMQP10Receiver(config)
        self.assertEqual('cert_path', receiver.cert_path)
        self.assertEqual(['url1', 'url2'], receiver.urls)
        self.assertEqual(['topic1', 'topic2'], receiver.topics)
        self.assertEqual('foo', receiver.receiver_name)

    @mock.patch('cki.cki_tools.amqp_bridge.AMQP10Receiver._parse_queues')
    def test_init_topic_replacement(self, mock_parse):
        """Test that init calls _parse_queues."""
        mock_parse.return_value = 'parse_return'
        config = {
            'name': 'foo',
            'cert_path': 'cert_path',
            'receiver_urls': ['url1', 'url2'],
            'message_topics': ['topic1', 'topic2'],
        }

        receiver = amqp_bridge.AMQP10Receiver(config)
        mock_parse.assert_called_with(['topic1', 'topic2'])
        self.assertEqual('parse_return', receiver.topics)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_parse_queues_staging(self):
        # pylint: disable=protected-access
        """Test _parse_queues. IS_PRODUCTION=False."""
        originals = [
            'queue://Consumer.msg-cki-bot-prod.amqp-bridge.VirtualTopic.eng.brew.task.closed.>',
            'topic://VirtualTopic.eng.cki.results.>',
        ]
        parsed = [
            'topic://VirtualTopic.eng.brew.task.closed.>',
            'topic://VirtualTopic.eng.cki.results.>'
        ]

        self.assertEqual(parsed, amqp_bridge.AMQP10Receiver._parse_queues(originals))

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_parse_queues_production(self):
        # pylint: disable=protected-access
        """Test _parse_queues. IS_PRODUCTION=True."""
        originals = [
            'queue://Consumer.msg-cki-bot-prod.amqp-bridge.VirtualTopic.eng.brew.task.closed.>',
            'topic://VirtualTopic.eng.cki.results.>',
        ]

        self.assertEqual(originals, amqp_bridge.AMQP10Receiver._parse_queues(originals))

    def _test_routing_key(self, address, expected):
        config = {
            'name': 'foo',
            'cert_path': 'cert_path',
            'receiver_urls': ['url1', 'url2'],
            'message_topics': ['topic1', 'topic2'],
        }

        receiver = amqp_bridge.AMQP10Receiver(config)
        event = mock.Mock(message=mock.Mock(headers={}, body='{}', address=address))
        with mock.patch('cki.cki_tools.amqp_bridge.message_send') as send:
            if not expected:
                self.assertRaises(Exception, receiver.callback, event)
            else:
                receiver.callback(event)
                send.assert_called_with({}, f'foo.{expected}', mock.ANY)

    def test_routing_key_translation(self):
        """Test routing key translation."""
        self._test_routing_key(
            'queue://Consumer.msg-cki-bot-prod.amqp-bridge.VirtualTopic.eng.brew.task.closed.>',
            'VirtualTopic.eng.brew.task.closed.>')
        self._test_routing_key(
            'topic://VirtualTopic.eng.brew.task.closed.>',
            'VirtualTopic.eng.brew.task.closed.>')
        self._test_routing_key(
            'queue://Consumer.a.b.c.VirtualTopic.eng.brew.task.closed.>',
            None)

    def _test_body(self, body, expected):
        config = {
            'name': 'foo',
            'cert_path': 'cert_path',
            'receiver_urls': ['url1', 'url2'],
            'message_topics': ['topic1', 'topic2'],
        }

        receiver = amqp_bridge.AMQP10Receiver(config)
        event = mock.Mock(message=mock.Mock(headers={}, body=body,
                          address='topic://VirtualTopic.eng.brew.task.closed.>'))
        with mock.patch('cki.cki_tools.amqp_bridge.message_send') as send:
            receiver.callback(event)
            if expected is None:
                send.assert_not_called()
            else:
                send.assert_called_with(
                    expected, 'foo.VirtualTopic.eng.brew.task.closed.>', mock.ANY)

    def test_bodies(self):
        """Test handling different body varieties."""
        self._test_body('{}', {})
        self._test_body({}, {})
        self._test_body('invalid', None)
        self._test_body(None, None)


class TestAMQP091Receiver(unittest.TestCase):
    """Test AMQP091Receiver."""

    @mock.patch('cki.cki_tools.amqp_bridge.messagequeue.MessageQueue')
    def test_init(self, messagequeue):
        """Test configs are correctly loaded."""
        config = {
            'name': 'foo',
            'host': 'host',
            'port': '123',
            'cafile': 'cafile',
            'certfile': 'certfile',
            'virtual_host': 'virtual_host',
            'exchange': 'exchange',
            'routing_keys': ['routing_key'],
            'queue_name': 'queue_name',
        }

        receiver = amqp_bridge.AMQP091Receiver(config)

        messagequeue.assert_called_with(
            host='host',
            port='123',
            cafile='cafile',
            certfile='certfile',
            virtual_host='virtual_host',
            dlx_retry=False
        )

        self.assertEqual('exchange', receiver.exchange)
        self.assertEqual(['routing_key'], receiver.routing_keys)
        self.assertEqual('queue_name', receiver.queue_name)
        self.assertEqual('foo', receiver.receiver_name)
