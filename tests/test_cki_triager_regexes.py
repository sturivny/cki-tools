"""Test regexes."""
from dataclasses import dataclass
import unittest

from datawarehouse import Datawarehouse
import responses

from cki.triager import regexes
from cki.triager.settings import NOT_FOUND


@dataclass
class TestMock:
    """Mock TestRun."""

    __test__ = False  # To supress a pytest warning
    name: str
    comment: str
    tree_name: str = 'foo'
    architecture: str = 'bar'
    checkout_id: int = 1
    build_id: int = 1


@unittest.mock.patch('cki.triager.regexes.dw_client', Datawarehouse('http://datawarehouse'))
class TestRegexChecker(unittest.TestCase):
    """Test TestRegexChecker."""

    def setUp(self):
        """setUp."""
        self.checker = regexes.RegexChecker()
        self.checker._download_lookups.cache_clear()  # pylint: disable=protected-access
        regexes.utils.get_build.cache_clear()
        regexes.utils.get_checkout.cache_clear()

    @responses.activate
    def test_search(self):
        """Test search."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": None
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [lookup]})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        self.assertEqual(
            [{'name': 'Bug description', 'id': 64, 'regex_id': 2}],
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

        # If text is empty, it should not match.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: None,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

    @responses.activate
    def test_search_no_result(self):
        """Test search with no result."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": None
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [lookup]})

        self.checker.download_lookups()

        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: "Text with no matches.",
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

    @responses.activate
    def test_search_test_name(self):
        """Test search with test_name."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": "test-name"
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [lookup]})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

        self.assertEqual(
            [{'name': 'Bug description', 'id': 64, 'regex_id': 2}],
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('test-path', 'Test with test-name in the name.')
            )
        )

        # If test_name is empty, it should not match.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('test-path', None)
            )
        )

    @responses.activate
    def test_search_file_name(self):
        """Test search with file_name."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": 'file.name',
            "test_name_match": None
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [lookup]})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

        self.assertEqual(
            [{'name': 'Bug description', 'id': 64, 'regex_id': 2}],
            self.checker.search(
                lambda: text,
                {'name': 'test_file.name', 'url': 'http://server/test_file.name'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

        # If file_name is empty, it should not match.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: text,
                {'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

    @responses.activate
    def test_search_file_name_and_test_name(self):
        """Test search with file_name and test_name."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": 'file.name',
            "test_name_match": 'test-name'
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [lookup]})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""

        # test_name and file_name wrong, text ok.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

        # test_name ok, file_name wrong, text ok.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'Test with test-name in the name.')
            )
        )

        # test_name wrong, file_name ok, text ok.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: text,
                {'name': 'file.name', 'url': 'http://server/file.name'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

        # test_name ok, file_name ok, text wrong.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: 'wrong text',
                {'name': 'file.name', 'url': 'http://server/file.name'},
                TestMock('test-path', 'Test with test-name in the name.')
            )
        )

        # All ok.
        self.assertEqual(
            [{'name': 'Bug description', 'id': 64, 'regex_id': 2}],
            self.checker.search(
                lambda: text,
                {'name': 'file.name', 'url': 'http://server/file.name'},
                TestMock('test-path', 'Test with test-name in the name.')
            )
        )

    @responses.activate
    def test_search_regex_syntax(self):
        """Test search with some regex syntax."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": "some.*thing|console.log",
            "test_name_match": "this-name|other-name",
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [lookup]})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        self.assertEqual(
            [{'name': 'Bug description', 'id': 64, 'regex_id': 2}],
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('this-name', 'this-name')
            )
        )

        self.assertEqual(
            [{'name': 'Bug description', 'id': 64, 'regex_id': 2}],
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('other-name', 'other-name')
            )
        )

        self.assertEqual(
            [{'name': 'Bug description', 'id': 64, 'regex_id': 2}],
            self.checker.search(
                lambda: text,
                {'name': 'something', 'url': 'http://server/something'},
                TestMock('other-name', 'other-name')
            )
        )

        self.assertEqual(
            [{'name': 'Bug description', 'id': 64, 'regex_id': 2}],
            self.checker.search(
                lambda: text,
                {'name': 'somebleblething', 'url': 'http://server/somebleblething'},
                TestMock('other-name', 'other-name')
            )
        )

    @responses.activate
    def test_search_with_arch(self):
        """Test search for a given architecture."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": None,
            "architecture_match": 'x86_64'
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [lookup]})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        self.assertEqual(
            [{'name': 'Bug description', 'id': 64, 'regex_id': 2}],
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng', architecture='x86_64')
            )
        )

        responses.add(responses.GET,
                      'http://datawarehouse/api/1/kcidb/builds/1',
                      json={'id': 1, 'architecture': 'x86_64',
                            'misc': {'iid': 1}})

        self.assertEqual(
            [{'name': 'Bug description', 'id': 64, 'regex_id': 2}],
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng',
                         architecture=None,
                         checkout_id=None)
            )
        )

    @responses.activate
    def test_search_with_arch_no_result(self):
        """Test search for a given architecture with no match."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": None,
            "architecture_match": 's390x|ppc64'
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [lookup]})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        # architecture is None.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng', architecture=None)
            )
        )
        # architecture wrong.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng', architecture='x86_64')
            )
        )

        # Need to fetch the build: architecture wrong.
        responses.add(responses.GET,
                      'http://datawarehouse/api/1/kcidb/builds/1',
                      json={'id': 1, 'architecture': 'foobar',
                            'misc': {'iid': 1}})

        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng',
                         architecture=None,
                         checkout_id=None)
            )
        )

    @responses.activate
    def test_search_with_tree(self):
        """Test search for a given tree."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": None,
            "tree_match": 'rhel8[46]-z$'
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [lookup]})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        # tree_name ok.
        self.assertEqual(
            [{'name': 'Bug description', 'id': 64, 'regex_id': 2}],
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng', tree_name='rhel86-z')
            )
        )

    @responses.activate
    def test_search_with_tree_no_result(self):
        """Test search for a given tree with no match."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": None,
            "tree_match": 'rawhide'
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [lookup]})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        # tree_name wrong.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng', tree_name='foo')
            )
        )

        # Need to fetch the checkout: tree_name wrong.
        responses.add(responses.GET, 'http://datawarehouse/api/1/kcidb/checkouts/1',
                      json={'id': 1, 'tree_name': 'foo', 'misc': {'iid': 1}})
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng', tree_name=None)
            )
        )

    @responses.activate
    def test_search_with_kpet_tree_name(self):
        """Test search for a given kpet tree name (release)."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": None,
            "architecture_match": None,
            "tree_match": None,
            "kpet_tree_name_match": 'foo'
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [lookup]})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""

        # Need to fetch the build: kpet_tree_name ok.
        responses.add(responses.GET,
                      'http://datawarehouse/api/1/kcidb/builds/1',
                      json={'id': 1,
                            'misc': {'iid': 1, 'kpet_tree_name': 'foo'}})

        self.assertEqual(
            [{'name': 'Bug description', 'id': 64, 'regex_id': 2}],
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng',
                         checkout_id=None)
            )
        )

    @responses.activate
    def test_search_with_kpet_tree_name_no_result(self):
        """Test search for a given kept tree name with no match."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": None,
            "architecture_match": None,
            "tree_match": None,
            "kpet_tree_name_match": 'foo'
        }

        responses.add(responses.GET,
                      'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [lookup]})

        # Need to fetch the build: kpet_tree_name wrong.
        responses.add(responses.GET,
                      'http://datawarehouse/api/1/kcidb/builds/1',
                      json={'id': 1,
                            'misc': {'iid': 1, 'kpet_tree_name': 'bar'}})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                lambda: text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng',
                         checkout_id=None)
            )
        )

    @responses.activate
    def test_download_lookups(self):
        """Test download_lookups."""
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [{"id": 1}, {"id": 2}, {"id": 3}]})

        self.checker.download_lookups()

        self.assertEqual(3, len(self.checker.lookups))
        self.assertEqual([1, 2, 3], [lookup.id for lookup in self.checker.lookups])

    @responses.activate
    def test_download_lookups_selective(self):
        """Test download_lookups, only specified ones."""
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex/1', json={'id': 1})
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex/3', json={'id': 3})

        self.checker.download_lookups(issueregex_ids=[1, 3])

        self.assertEqual(2, len(self.checker.lookups))
        self.assertEqual([1, 3], [lookup.id for lookup in self.checker.lookups])
