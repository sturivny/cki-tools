"""KCIDB utils tests."""
# pylint: disable=no-member
import unittest

from botocore.exceptions import ClientError
import responses

from cki.kcidb.utils import is_empty
from cki.kcidb.utils import patch_list_hash
from cki.kcidb.utils import timestamp_to_iso
from cki.kcidb.utils import tzaware_str2utc
from cki.kcidb.utils import upload_file
from tests.mocks import get_mocked_bucket


class TestUtils(unittest.TestCase):
    """Test Job methods."""

    @responses.activate
    def test_upload_empty(self):
        """Test upload_file with empty content and no source_path."""
        self.assertIsNone(upload_file('public', 'path', 'file.name'))

    @responses.activate
    def test_upload_file_content(self):
        """
        Test upload_file method via file_content.

        File is not in remote. Uploading from file_content.
        """
        mocked_bucket = get_mocked_bucket()

        with unittest.mock.patch('cki.kcidb.utils.BUCKETS', {'public': mocked_bucket}):
            mocked_bucket.client.head_object.side_effect = ClientError({'Error': {'Code': '404'}},
                                                                       '')

            result_path = upload_file('public', 'path-mock', 'file.name', file_content='content')
            self.assertEqual('endpoint/bucket/prefix/path-mock/file.name', result_path)

            mocked_bucket.client.head_object.assert_called_with(
                Bucket='bucket', Key='prefix/path-mock/file.name')
            mocked_bucket.client.put_object.assert_called_with(
                Body='content', Bucket='bucket',
                ContentType='text/plain', Key='prefix/path-mock/file.name')

    def test_upload_file_source_path(self):
        """
        Test upload_file method via source_path.

        File is not in remote. Uploading from source_path.
        """
        mocked_bucket = get_mocked_bucket()
        mocked_bucket.client.head_object.side_effect = ClientError({'Error': {'Code': '404'}}, '')
        with unittest.mock.patch('cki.kcidb.utils.BUCKETS',
                                 {'private': mocked_bucket}):

            upload_file('private', 'path-mock', 'file.name', source_path='/file/path')

            mocked_bucket.client.head_object.assert_called_with(
                Bucket='bucket', Key='prefix/path-mock/file.name')
            mocked_bucket.client.upload_file.assert_called_with(
                Bucket='bucket', Filename='/file/path',
                Key='prefix/path-mock/file.name', ExtraArgs={'ContentType': 'text/plain'})

    def test_upload_file_in_remote(self):
        """Test upload_file method. File is in remote."""
        mocked_bucket = get_mocked_bucket()
        with unittest.mock.patch('cki.kcidb.utils.BUCKETS', {'private': mocked_bucket}):

            upload_file('private', 'path-mock', 'file.name', source_path='/file/path')

            mocked_bucket.client.head_object.assert_called_with(
                Bucket='bucket', Key='prefix/path-mock/file.name')
            self.assertFalse(mocked_bucket.client.put_object.called)
            self.assertFalse(mocked_bucket.client.upload_file.called)

    @unittest.mock.patch('cki.kcidb.utils.BUCKETS', {'private': unittest.mock.Mock()})
    @unittest.mock.patch('cki.kcidb.utils.LOGGER.warning')
    def test_upload_file_fail(self, mock_warning):
        """Ensure upload_file sanitizes its arguments."""
        result = upload_file('private', 'path', None, None)
        self.assertIsNone(result)
        mock_warning.assert_called_with('File "%s" is empty', None)

    def test_is_empty(self):
        """Test is_empty method."""
        test_cases = [
            (None, True),
            ('', False),
            ('something', False),
            ([], True),
            ([''], False),
            ({}, True),
            ({'a': 'b'}, False),
            (False, False),
        ]

        for case, expected in test_cases:
            self.assertEqual(expected, is_empty(
                case), (case, expected))

    def test_timestamp_as_iso(self):
        """Test timestamp_to_iso."""
        test_cases = [
            ('2020-05-18', '2020-05-18T00:00:00+00:00'),
            ('2020-05-18 17:33:59', '2020-05-18T17:33:59+00:00'),
            ('2020-05-18T17:33:59Z', '2020-05-18T17:33:59+00:00'),
            ('2020-05-18 17:33:59 +2', '2020-05-18T17:33:59+02:00'),
            ('2020-05-18T17:33:59+00:00', '2020-05-18T17:33:59+00:00'),
            ('2020-05-18T17:33:59+01:00', '2020-05-18T17:33:59+01:00'),
        ]

        for timestamp, expected in test_cases:
            self.assertEqual(expected, timestamp_to_iso(timestamp))

    def test_tzaware_str2utc(self):
        test_cases = (
            ('2021-04-13T14:33:08-04:00', '2021-04-13T18:33:08+00:00'),
            ('2021-04-13 10:30:07 +0000', '2021-04-13T10:30:07+00:00'),
        )

        for tzaware_str, expected in test_cases:
            self.assertEqual(expected, tzaware_str2utc(tzaware_str))

    @responses.activate
    def test_patchset_hash(self):
        """Test patchset_hash return values."""
        responses.add(responses.GET, 'https://s/p_1', body=b'p_1')
        responses.add(responses.GET, 'https://s/p_2/', body=b'p_2')

        patch_list = [
            'https://s/p_1', 'https://s/p_2/'
        ]

        self.assertEqual('', patch_list_hash([]))

        self.assertEqual(
            'a560b2642c43bb3494b79d364d9371dd4468307443d1fde7b12f51e7ba4c1b33',
            patch_list_hash([patch_list[0]])
        )
        self.assertEqual(
            '8d2e6de35099fc8e1c9f98f6c107c004f414846d30f6315a23ccbe0d9a664328',
            patch_list_hash(patch_list)
        )
