"""Gitlab api interaction tests."""
import hashlib
import hmac
import json
import os
import unittest
from unittest import mock

import pika

from cki.cki_tools.webhook_receiver import flask
from cki.cki_tools.webhook_receiver import lambdas
from cki.cki_tools.webhook_receiver import receiver

SECRET = 'secret'
UUID = '12345678-9012-3456-7890-123456789012'

# disable keepalive on the queue
receiver.QUEUE.keepalive_s = 0


@mock.patch.dict(os.environ, {'WEBHOOK_RECEIVER_WEBSECRET': SECRET})
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
class TestWebhook(unittest.TestCase):
    """ Test webhook receiver."""

    def setUp(self):
        """SetUp class."""
        self.client = flask.app.test_client()
        receiver.TRY_ENDLESSLY = False  # set to True by import of lambdas module

    def _check_send(self, event, data):
        response = self.client.post(
            '/', json=data, headers={'X-Gitlab-Token': SECRET,
                                     'X-Gitlab-Event': event,
                                     'X-Gitlab-Event-UUID': UUID})
        self.assertEqual(response.status, '200 OK')

    @staticmethod
    def _publish_call(event, data, routing_key):
        properties = pika.BasicProperties(
            delivery_mode=2,
            headers={
                'message-type': 'gitlab',
                'message-gitlab-event': event,
                'message-gitlab-event-uuid': UUID})
        return mock.call(
            exchange=receiver.RABBITMQ_EXCHANGE,
            routing_key=routing_key,
            body=json.dumps(data),
            properties=properties)

    def _check_event(self, event, data, routing_key, connection):
        self._check_send(event, data)
        self.assertEqual(connection().channel().basic_publish.mock_calls,
                         [self._publish_call(event, data, routing_key)])

    @mock.patch('pika.BlockingConnection')
    def test_job_event(self, connection):
        """Check routing of a job event."""
        self._check_event('Job Hook', {
            'object_kind': 'build',
            'repository': {
                'homepage': 'https://host.name:1234/group/subgroup/project',
            }
        }, 'host.name.group.subgroup.project.build', connection)

    @mock.patch('pika.BlockingConnection')
    def test_pipeline_event(self, connection):
        """Check routing of a pipeline event."""
        self._check_event('Pipeline Hook', {
            'object_kind': 'pipeline',
            'project': {
                'web_url': 'https://host.name:1234/group/subgroup/project',
            },
        }, 'host.name.group.subgroup.project.pipeline', connection)

    def test_entrypoint_missing(self):
        """Check a missing entrypoint."""
        response = self.client.post('/non-existent', json={})
        self.assertEqual(response.status, '404 NOT FOUND')

    def test_websecret_missing(self):
        """Check a missing websecret."""
        response = self.client.post('/', json={})
        self.assertEqual(response.status, '403 FORBIDDEN')

    def test_websecret_wrong(self):
        """Check a wrong websecret."""
        response = self.client.post(
            '/', json={}, headers={'X-Gitlab-Token': 'bad_secret'})
        self.assertEqual(response.status, '403 FORBIDDEN')

    @mock.patch.dict(os.environ, {'WEBHOOK_RECEIVER_WEBSECRET': ''})
    def test_websecret_env_missing(self):
        """Check a missing websecret env variable."""
        response = self.client.post('/', json={})
        self.assertEqual(response.status, '404 NOT FOUND')

    def test_missing_object_kind(self):
        """Check a missing object_kind."""
        response = self.client.post(
            '/', json={}, headers={'X-Gitlab-Token': SECRET})
        self.assertEqual(response.status, '500 INTERNAL SERVER ERROR')

    @mock.patch('pika.BlockingConnection')
    def test_errors(self, connection):
        """Check transient error handling."""
        routing_key = 'host.group.subgroup.project.build'
        messages = [{
            'index': index,
            'object_kind': 'build',
            'repository': {
                'homepage': 'https://host:1234/group/subgroup/project',
            },
        } for index in range(3)]

        connection().channel().basic_publish.side_effect = [OSError('error')] * 4 + [True] * 3

        self._check_send('Job Hook', messages[0])
        self._check_send('Job Hook', messages[1])
        self.assertEqual(connection().channel().basic_publish.mock_calls, [
            self._publish_call('Job Hook', messages[0], routing_key)] * 4)

        connection().channel().basic_publish.mock_calls = []
        self._check_send('Job Hook', messages[2])
        self.assertEqual(connection().channel().basic_publish.mock_calls, [
            self._publish_call('Job Hook', messages[i], routing_key)
            for i in range(3)])

    @mock.patch('pika.BlockingConnection')
    def test_lambda(self, connection):
        """Check that queryStringParameters is not needed."""
        response = lambdas.gitlab_lambda({
            'headers': {'X-Gitlab-Token': SECRET,
                        'X-Gitlab-Event': 'pipeline'},
            'body': json.dumps({
                'object_kind': 'pipeline',
                'project': {
                    'web_url': 'https://host.name:1234/group/subgroup/project',
                },
            })}, None)
        self.assertEqual(response['statusCode'], 200)


@mock.patch.dict(os.environ, {'WEBHOOK_RECEIVER_SENTRY_IO_CLIENT_SECRET': SECRET})
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
class TestSentry(unittest.TestCase):
    """Test Sentry webhook receiver."""

    def setUp(self):
        """SetUp class."""
        self.client = flask.app.test_client()

    def _check_send(self, resource, data):
        data = json.dumps(data).encode('utf8')
        signature = hmac.new(
            key=SECRET.encode('utf8'),
            msg=data,
            digestmod=hashlib.sha256,
        ).hexdigest()
        response = self.client.post(
            '/sentry',
            data=data, content_type='application/json',
            headers={'Sentry-Hook-Resource': resource,
                     'Sentry-Hook-Signature': signature})
        self.assertEqual(response.status, '200 OK')

    @staticmethod
    def _publish_call(resource, data, routing_key):
        properties = pika.BasicProperties(
            delivery_mode=2,
            headers={
                'message-type': 'sentry',
                'message-sentry-resource': resource})
        return mock.call(
            exchange=receiver.RABBITMQ_EXCHANGE,
            routing_key=routing_key,
            body=json.dumps(data),
            properties=properties)

    def _check_event(self, resource, data, routing_key, connection):
        self._check_send(resource, data)
        if routing_key:
            self.assertEqual(connection().channel().basic_publish.mock_calls, [
                self._publish_call(resource, data, routing_key)])
        else:
            self.assertFalse(connection().channel().basic_publish.mock_calls)

    @mock.patch('pika.BlockingConnection')
    def test_issue(self, connection):
        """Check routing of an issue event."""
        self._check_event('issue', {
            'action': 'created',
            'data': {'issue': {
                'project': {'slug': 'project-slug'}
            }}
        }, 'sentry.io.project-slug.issue.created', connection)

    @mock.patch('pika.BlockingConnection')
    def test_event(self, connection):
        """Check routing of an event alert."""
        self._check_event('event_alert', {
            'action': 'triggered',
            'data': {'event': {
                'url': 'https://sentry.io/api/0/projects/org/project/events/1/'
            }}
        }, 'sentry.io.project.event_alert.triggered', connection)

    @mock.patch('pika.BlockingConnection')
    def test_unknown(self, connection):
        """Check ignoring of an unknown event."""
        self._check_event('unknown', {},
                          None, connection)

    def test_websecret_missing(self):
        """Check a missing signature."""
        response = self.client.post('/sentry', json={})
        self.assertEqual(response.status, '403 FORBIDDEN')

    def test_websecret_wrong(self):
        """Check a wrong websecret."""
        response = self.client.post(
            '/sentry', json={}, headers={'Sentry-Hook-Signature': 'invalid'})
        self.assertEqual(response.status, '403 FORBIDDEN')

    @mock.patch.dict(os.environ,
                     {'WEBHOOK_RECEIVER_SENTRY_IO_CLIENT_SECRET': ''})
    def test_websecret_env_missing(self):
        """Check a missing websecret env variable."""
        response = self.client.post('/sentry', json={})
        self.assertEqual(response.status, '404 NOT FOUND')


@mock.patch.dict(os.environ, {'WEBHOOK_RECEIVER_WEBSECRET': SECRET})
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
class TestGitlabWebhookTopics(unittest.TestCase):
    """Test the topic formatting."""

    def _test(self, topic_format, expected, object_kind='push'):
        with mock.patch('cki.cki_tools.webhook_receiver.receiver.RABBITMQ_GITLAB_TOPIC_TEMPLATE',
                        topic_format), mock.patch('pika.BlockingConnection') as connection:
            exception = None
            try:
                result = receiver.gitlab_handler(
                    {'X-Gitlab-Token': SECRET, 'X-Gitlab-Event': object_kind},
                    json.dumps({
                        'object_kind': object_kind,
                        'repository': {
                            'homepage': 'https://gitlab.com/kernelci/kernel-webhooks',
                        }}).encode('utf8'))
            # pylint: disable=broad-except
            except Exception as exc:
                exception = exc
            if not isinstance(expected, str):
                self.assertIsInstance(exception, expected)
            else:
                self.assertEqual(result[0], 200)
                self.assertEqual(connection().channel().basic_publish
                                 .mock_calls[0].kwargs['routing_key'], expected)

    @mock.patch.dict(os.environ, {'RABBITMQ_ENV': 'stg'})
    def test_parse_env_var(self):
        """Test env var retrieval."""
        self._test('{env[RABBITMQ_ENV]}', 'stg')

    def test_parse_env_var_undefined(self):
        """Test the retrieval of a non-existing env var."""
        self._test('{env[RABBITMQ_ENV]}', KeyError)

    def test_parse_ns_var(self):
        """Test namespace var retrieval."""
        self._test('{object_kind}', 'push', object_kind='push')

    def test_parse_ns_var_undefined(self):
        """Test the retrieval of a non-existing namespace var."""
        self._test('{unknown}', KeyError)

    def test_parsed_default(self):
        """Test the default topic formatting."""
        self._test(receiver.RABBITMQ_GITLAB_TOPIC_TEMPLATE,
                   'gitlab.com.kernelci.kernel-webhooks.push')

    @mock.patch.dict(os.environ, {'RABBITMQ_ENV': 'stg'})
    def test_parsed_fedora_messaging(self):
        """Test topic format for fedora messaging."""
        self._test('.'.join([
            'org',
            'centos',
            '{env[RABBITMQ_ENV]}',
            'gitlab',
            '{web_url.path}',
            '{object_kind}'
        ]), 'org.centos.stg.gitlab.kernelci.kernel-webhooks.push')

    def test_parsed_undefined_env_var(self):
        """Test missing env vars for a parsed topic."""
        self._test('.'.join([
            'org',
            'centos',
            '${env[RABBITMQ_ENV]}',
            'gitlab',
            '{web_url_path}',
            '{object_kind}'
        ]), KeyError)

    def test_parsed_undefined_ns_var(self):
        """Test missing namespace vars for a parsed topic."""
        self._test('.'.join([
            'org',
            'centos',
            'gitlab',
            '${web_url.path}',
            '${unknown}'
        ]), KeyError)


@mock.patch.dict(os.environ, {'WEBHOOK_RECEIVER_WEBSECRET': SECRET})
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
class TestJira(unittest.TestCase):
    """Test Jira webhook receiver."""

    def setUp(self):
        """SetUp class."""
        self.client = flask.app.test_client()

    def _check_send(self, data):
        data = json.dumps(data).encode('utf8')
        response = self.client.post(
            f'/jira?token={SECRET}',
            data=data, content_type='application/json')
        self.assertEqual(response.status, '200 OK')

    @staticmethod
    def _publish_call(data, routing_key):
        properties = pika.BasicProperties(
            delivery_mode=2,
            headers={
                'message-type': 'jira',
            })
        return mock.call(
            exchange=receiver.RABBITMQ_EXCHANGE,
            routing_key=routing_key,
            body=json.dumps(data),
            properties=properties)

    def _check_event(self, data, routing_key, connection):
        self._check_send(data)
        self.assertEqual(connection().channel().basic_publish.mock_calls, [
            self._publish_call(data, routing_key)])

    @mock.patch('pika.BlockingConnection')
    def test_issue(self, connection):
        """Check routing of an issue event."""
        self._check_event(
            {
                'issue_event_type_name': 'issue_created',
                'issue': {
                    'self': 'https://some.jira.server/rest/api/2/issue/1234',
                    'fields': {
                        'project': {
                            'key': 'project-slug'
                        }
                    }
                }
            },
            'some.jira.server.project-slug.issue_created',
            connection
        )

    def test_websecret_missing(self):
        """Check a missing websecret."""
        response = self.client.post('/jira')
        self.assertEqual(response.status, '403 FORBIDDEN')

    def test_websecret_wrong(self):
        """Check a wrong websecret."""
        response = self.client.post('/jira?token=foo')
        self.assertEqual(response.status, '403 FORBIDDEN')

    @mock.patch.dict(os.environ, {'WEBHOOK_RECEIVER_WEBSECRET': ''})
    def test_websecret_env_missing(self):
        """Check a missing websecret env variable."""
        response = self.client.post('/jira?token=foo')
        self.assertEqual(response.status, '404 NOT FOUND')
