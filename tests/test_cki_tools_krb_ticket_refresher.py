"""Test krb_ticket_refresher script."""
import unittest
from unittest import mock

from cki.cki_tools import krb_ticket_refresher


class TestRefreshKerberosTicket(unittest.TestCase):
    """Test RefreshKerberosTicket class."""

    @mock.patch('cki.cki_tools.krb_ticket_refresher.subprocess.run')
    def test_run(self, mock_subprocess):
        """Test run function."""
        krb_ticket_refresher.RefreshKerberosTicket().run()
        mock_subprocess.assert_called_with(['/usr/bin/kinit', '-R'], check=True)

    @mock.patch('cki.cki_tools.krb_ticket_refresher.subprocess.run')
    def test_run_failure(self, mock_subprocess):
        """Test run function."""
        mock_subprocess.side_effect = Exception
        self.assertRaises(Exception, krb_ticket_refresher.RefreshKerberosTicket().run)
