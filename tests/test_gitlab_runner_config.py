"""Tests for gitlab runner config."""
import contextlib
import io
import json
import pathlib
import tempfile
import unittest
from unittest import mock

import responses
import toml
import yaml

from cki.deployment_tools.gitlab_runner_config import main


class TestRunnerConfig(unittest.TestCase):
    """Base class with a common test bed."""

    maxDiff = 10000
    config = {
        'runner_tokens': {
            'c1-d1-kubernetes-low': 'runner-token-c1-d1-kubernetes-low',
            'c1-d2-docker-high': 'runner-token-c1-d2-docker-high',
            'c1-d2-docker-low': 'runner-token-c1-d2-docker-low',
            'c1-d2-docker-priv': 'runner-token-c1-d2-docker-priv',
            'c2-d2-docker-low': 'runner-token-c2-d2-docker-low',
            'c2-d2-docker-priv': 'runner-token-c2-d2-docker-priv',
        },
        'gitlab_instances': {
            'i1': {'url': 'https://url1', 'api_token': 'token1'},
            'i2': {'url': 'https://url2', 'api_token': 'token2'},
        },
        'runner_deployments': {
            '.default': {'.metadata': {'name': 'gitlab-runner', },
                         '.filename': 'config.toml'},
            'd1': {'.executor': 'kubernetes', '.secret': 'd1.yaml'},
            'd2': {'.executor': 'docker', '.configfile': 'd2.toml'}
        },
        'runner_templates': {
            'docker-high': {'executor': 'docker'},
            'docker-low': {'executor': 'docker', '.tag_list': 'r1'},
            'docker-priv': {'executor': 'docker', '.tag_list': 'r2'},
            'kubernetes-low': {'executor': 'kubernetes', 'key': 'value', '.tag_list': 'r3'}
        },
        'variable_groups': {
            'var1': {'key': 'value', 'key2': 'value2'},
            'var2': {'.extends': 'var1', 'key2': 'value3'},
            'var3': {'key3': 'value3'},
        },
        'runner_configurations': {
            'c1': {
                'runner_deployments': {
                    'd1': ['kubernetes-low'],
                    'd2': ['docker-high', 'docker-low', 'docker-priv']},
                'variable_group': 'var1',
                'gitlab_groups': 'i1/g1',
            },
            'c2': {
                'runner_deployments': {
                    'd1': [],
                    'd2': ['docker-low', 'docker-priv']
                },
                'gitlab_projects': ['i1/g1/p1', 'i1/g1/p2'],
            }
        },
        'gitlab_variables': [
            {'gitlab_groups': 'i1/g1', 'variable_group': 'var1'},
            {'gitlab_projects': 'i1/g1/p1', 'variable_group': 'var2'},
            {'gitlab_projects': 'i2/g2/p2', 'variable_group': 'var3'},
        ],
        'webhook_endpoints': [{
            'url': 'https://host.url/path',
            '.pattern': 'https://(host.url|other.url)/path',
            '.secret_token': 'really-secret',
            '.routing_keys': ['url1.g1.p1.build', 'url1.g1.*.deployment',
                              'url1.g1.*.merge_request',
                              'url1.g1.*.issue',
                              'url2.g2.test-#.pipeline',
                              'url1.g1.p1.pipeline url1.g1.p1.merge_request']
        }, {
            'url': 'https://more.url/path',
            '.secret_token': 'really-really-secret',
            '.routing_keys': ['url1.g1.p1.build', 'url2.g2.l2.p2.pipeline']
        }]
    }

    def setUp(self):
        responses.add(responses.GET,
                      url='https://url1/api/v4/groups/g1/variables',
                      json=[{'key': 'key', 'value': 'value'}])
        responses.add(responses.GET,
                      url='https://url1/api/v4/projects/g1%2Fp1/variables',
                      json=[{'key': 'key', 'value': 'value', },
                            {'key': 'key2', 'value': 'value2'}])
        responses.add(responses.GET,
                      url='https://url2/api/v4/projects/g2%2Fp2/variables',
                      json=[{'key': 'key3', 'value': 'value3'},
                            {'key': 'key4', 'value': 'value4'}])
        responses.add(responses.GET,
                      url='https://url1/api/v4/runners',
                      json=[
                          {'id': 1, 'description': 'c1-d2-docker-high',
                           'online': True, 'active': False},
                          {'id': 2, 'description': 'c1-d2-docker-low',
                           'online': True, 'active': True},
                          {'id': 3, 'description': 'c1-d2-docker-priv',
                           'online': True, 'active': True},
                          {'id': 4, 'description': 'c2-d2-docker-low',
                           'online': True, 'active': True}])
        responses.add(responses.GET,
                      url='https://url2/api/v4/runners',
                      json=[])
        responses.add(responses.GET,
                      url='https://url1/api/v4/runners/1',
                      json={'id': 1, 'description': 'c1-d2-docker-high',
                            'projects': [], 'tag_list': ['t1'],
                            'groups': [{'id': 1,
                                        'web_url': 'https://url1/groups/g1'}],
                            'access_level': 'not_protected',
                            'active': False,
                            'locked': True,
                            'run_untagged': False,
                            'maximum_timeout': 24 * 60 * 60})
        responses.add(responses.GET,
                      url='https://url1/api/v4/runners/2',
                      json={'id': 2, 'description': 'c1-d2-docker-low',
                            'projects': [], 'tag_list': ['r2'],
                            'groups': [{'id': 1,
                                        'web_url': 'https://url1/groups/g1'}],
                            'access_level': 'not_protected',
                            'active': True,
                            'locked': True,
                            'run_untagged': False,
                            'maximum_timeout': 24 * 60 * 60})
        responses.add(responses.GET,
                      url='https://url1/api/v4/runners/3',
                      json={'id': 3, 'description': 'c1-d2-docker-priv',
                            'projects': [], 'tag_list': [],
                            'groups': [{'id': 1,
                                        'web_url': 'https://url1/groups/g1'}],
                            'access_level': 'not_protected',
                            'active': True,
                            'locked': True,
                            'run_untagged': False,
                            'maximum_timeout': 24 * 60 * 60})
        responses.add(responses.GET,
                      url='https://url1/api/v4/runners/4',
                      json={'id': 4, 'description': 'c2-d2-docker-low',
                            'projects': [{'id': 1,
                                          'web_url': 'https://url1/g1/p1'},
                                         {'id': 3,
                                          'web_url': 'https://url1/g1/p3'}],
                            'groups': [], 'tag_list': [],
                            'access_level': 'not_protected',
                            'active': True,
                            'locked': True,
                            'run_untagged': False,
                            'maximum_timeout': 24 * 60 * 60})
        responses.add(responses.GET,
                      url='https://url1/api/v4/groups/1',
                      json={'full_path': 'g1'})
        responses.add(responses.GET,
                      url='https://url1/api/v4/groups/g1',
                      json={'full_path': 'g1', 'runners_token': 'g1-token'})
        responses.add(responses.GET,
                      url='https://url1/api/v4/groups/2',
                      json={'full_path': 'g2'})
        responses.add(responses.GET,
                      url='https://url2/api/v4/groups',
                      json=[{'full_path': 'g2'}])
        responses.add(responses.GET,
                      url='https://url2/api/v4/groups/g2',
                      json={'full_path': 'g2'})
        responses.add(responses.GET,
                      url='https://url1/api/v4/projects/1',
                      json={'path_with_namespace': 'g1/p1'})
        responses.add(responses.GET,
                      url='https://url1/api/v4/projects/g1%2Fp1',
                      json={'path_with_namespace': 'g1/p1', 'runners_token': 'p1-token'})
        responses.add(responses.GET,
                      url='https://url1/api/v4/projects/3',
                      json={'path_with_namespace': 'g1/p3'})
        responses.add(responses.GET,
                      url='https://url2/api/v4/projects/g2%2Fp2',
                      json={'path_with_namespace': 'g2/p2'})
        responses.add(responses.GET,
                      url='https://url2/api/v4/projects/g2%2Fl2%2Fp2',
                      status=404)
        responses.add(responses.GET,
                      url='https://url2/api/v4/projects/g2%2Fl2.p2',
                      json={'path_with_namespace': 'g2/l2.p2'})
        responses.add(responses.GET,
                      url='https://url1/api/v4/groups/g1/hooks',
                      json=[])
        responses.add(responses.GET,
                      url='https://url2/api/v4/groups/g2/hooks',
                      json=[])
        responses.add(responses.GET,
                      url='https://url1/api/v4/projects/g1%2Fp1/hooks',
                      json=[{'id': 1,
                             'url': 'https://other.url/path',
                             'alert_status': 'disabled',
                             }])
        responses.add(responses.GET,
                      url='https://url2/api/v4/projects/g2%2Fl2.p2/hooks',
                      json=[{'id': 1,
                             'url': 'https://more.url/path',
                             }])

    def _main(self, args):
        with tempfile.NamedTemporaryFile('w+t') as config_file:
            config_file.write(yaml.safe_dump(self.config))
            config_file.flush()
            with contextlib.redirect_stdout(io.StringIO()), \
                    contextlib.redirect_stderr(io.StringIO()):
                result = main(['--config', config_file.name] + args)
            if args[1] == 'diff':
                return result.strip().split('\n')
            if args[1] == 'apply':
                return None
            return yaml.safe_load(result)

    @staticmethod
    def _requests(method=None, url=None, parse=True):
        return [json.loads(c.request.body) if parse else c.request.body
                for c in responses.calls
                if (not method or c.request.method == method) and
                (not url or c.request.url == url)]


@mock.patch('cki_lib.misc.sentry_init', mock.Mock())
class TestWebhooks(TestRunnerConfig):
    """Tests the webhooks functionality."""

    @responses.activate
    def test_dump(self):
        """Check the dumping of webhooks."""
        with self.assertLogs(level='ERROR'):
            result = self._main(['webhooks', 'dump'])
        self.assertEqual(result, {
            'i1/g1/p1': {'https://host.url/path': {
                'url': 'https://other.url/path'}},
            'i2/g2/l2.p2': {'https://more.url/path': {
                'url': 'https://more.url/path'}}})

    @responses.activate
    def test_generate(self):
        """Check the generation of webhooks."""
        result = self._main(['webhooks', 'generate'])
        self.assertEqual(result, {

            'i1/g1': {
                'https://host.url/path': {
                    'deployment_events': True,
                    'enable_ssl_verification': True,
                    'issues_events': True,
                    'job_events': False,
                    'merge_requests_events': True,
                    'note_events': False,
                    'pipeline_events': False,
                    'push_events': False,
                    'subgroup_events': False,
                    'url': 'https://host.url/path',
                },
            },
            'i1/g1/p1': {
                'https://host.url/path': {
                    'enable_ssl_verification': True,
                    'deployment_events': False,
                    'issues_events': False,
                    'job_events': True,
                    'merge_requests_events': False,
                    'note_events': False,
                    'pipeline_events': True,
                    'push_events': False,
                    'url': 'https://host.url/path'
                },
                'https://more.url/path': {
                    'enable_ssl_verification': True,
                    'deployment_events': False,
                    'issues_events': False,
                    'job_events': True,
                    'merge_requests_events': False,
                    'note_events': False,
                    'pipeline_events': False,
                    'push_events': False,
                    'url': 'https://more.url/path'
                }
            },
            'i2/g2': {
                'https://host.url/path': {
                    'deployment_events': False,
                    'enable_ssl_verification': True,
                    'issues_events': False,
                    'job_events': False,
                    'merge_requests_events': False,
                    'note_events': False,
                    'pipeline_events': True,
                    'push_events': False,
                    'subgroup_events': True,
                    'url': 'https://host.url/path',
                },
            },
            'i2/g2/l2.p2': {
                'https://more.url/path': {
                    'enable_ssl_verification': True,
                    'deployment_events': False,
                    'issues_events': False,
                    'job_events': False,
                    'merge_requests_events': False,
                    'note_events': False,
                    'pipeline_events': True,
                    'push_events': False,
                    'url': 'https://more.url/path'
                }
            }})

    @responses.activate
    def test_diff(self):
        """Check the diffing of webhooks."""
        changes = self._main(['webhooks', 'diff'])
        self.assertEqual(changes, [
            '--- current',
            '',
            '+++ proposed',
            '',
            '@@ -1,7 +1,57 @@',
            '',
            '+i1/g1:',
            '+  https://host.url/path:',
            '+    deployment_events: true',
            '+    enable_ssl_verification: true',
            '+    issues_events: true',
            '+    job_events: false',
            '+    merge_requests_events: true',
            '+    note_events: false',
            '+    pipeline_events: false',
            '+    push_events: false',
            '+    subgroup_events: false',
            '+    url: https://host.url/path',
            ' i1/g1/p1:',
            '   https://host.url/path:',
            '-    url: https://other.url/path',
            '+    deployment_events: false',
            '+    enable_ssl_verification: true',
            '+    issues_events: false',
            '+    job_events: true',
            '+    merge_requests_events: false',
            '+    note_events: false',
            '+    pipeline_events: true',
            '+    push_events: false',
            '+    url: https://host.url/path',
            '+  https://more.url/path:',
            '+    deployment_events: false',
            '+    enable_ssl_verification: true',
            '+    issues_events: false',
            '+    job_events: true',
            '+    merge_requests_events: false',
            '+    note_events: false',
            '+    pipeline_events: false',
            '+    push_events: false',
            '+    url: https://more.url/path',
            '+i2/g2:',
            '+  https://host.url/path:',
            '+    deployment_events: false',
            '+    enable_ssl_verification: true',
            '+    issues_events: false',
            '+    job_events: false',
            '+    merge_requests_events: false',
            '+    note_events: false',
            '+    pipeline_events: true',
            '+    push_events: false',
            '+    subgroup_events: true',
            '+    url: https://host.url/path',
            ' i2/g2/l2.p2:',
            '   https://more.url/path:',
            '+    deployment_events: false',
            '+    enable_ssl_verification: true',
            '+    issues_events: false',
            '+    job_events: false',
            '+    merge_requests_events: false',
            '+    note_events: false',
            '+    pipeline_events: true',
            '+    push_events: false',
            '     url: https://more.url/path',
        ])

    @responses.activate
    def test_apply(self):
        """Check the application of webhooks."""
        post_url = 'https://url1/api/v4/projects/g1%2Fp1/hooks'
        post2_url = 'https://url1/api/v4/groups/g1/hooks'
        post3_url = 'https://url2/api/v4/groups/g2/hooks'
        put_url = 'https://url1/api/v4/projects/g1%2Fp1/hooks/1'
        put2_url = 'https://url2/api/v4/projects/g2%2Fl2.p2/hooks/1'
        responses.add(responses.POST, post_url, json={})
        responses.add(responses.POST, post2_url, json={})
        responses.add(responses.POST, post3_url, json={})
        responses.add(responses.PUT, put_url, json={})
        responses.add(responses.PUT, put2_url, json={})
        self._main(['webhooks', 'apply'])
        self.assertEqual(self._requests(method='POST', url=post_url), [{
            'enable_ssl_verification': True,
            'deployment_events': False,
            'issues_events': False,
            'job_events': True,
            'merge_requests_events': False,
            'note_events': False,
            'pipeline_events': False,
            'push_events': False,
            'token': 'really-really-secret',
            'url': 'https://more.url/path',
        }])
        self.assertEqual(self._requests(method='POST', url=post2_url), [{
            'enable_ssl_verification': True,
            'deployment_events': True,
            'issues_events': True,
            'job_events': False,
            'merge_requests_events': True,
            'note_events': False,
            'pipeline_events': False,
            'subgroup_events': False,
            'push_events': False,
            'token': 'really-secret',
            'url': 'https://host.url/path',
        }])
        self.assertEqual(self._requests(method='POST', url=post3_url), [{
            'enable_ssl_verification': True,
            'deployment_events': False,
            'issues_events': False,
            'job_events': False,
            'merge_requests_events': False,
            'note_events': False,
            'pipeline_events': True,
            'subgroup_events': True,
            'push_events': False,
            'token': 'really-secret',
            'url': 'https://host.url/path',
        }])
        self.assertEqual(self._requests(method='PUT', url=put_url), [{
            'enable_ssl_verification': True,
            'deployment_events': False,
            'issues_events': False,
            'job_events': True,
            'merge_requests_events': False,
            'note_events': False,
            'pipeline_events': True,
            'push_events': False,
            'token': 'really-secret',
            'url': 'https://host.url/path',
        }])
        self.assertEqual(self._requests(method='PUT', url=put2_url), [{
            'enable_ssl_verification': True,
            'deployment_events': False,
            'issues_events': False,
            'job_events': False,
            'merge_requests_events': False,
            'note_events': False,
            'pipeline_events': True,
            'push_events': False,
            'token': 'really-really-secret',
            'url': 'https://more.url/path',
        }])


@mock.patch('cki_lib.misc.sentry_init', mock.Mock())
class TestVariables(TestRunnerConfig):
    """Tests the variables functionality."""

    @responses.activate
    def test_dump(self):
        """Check the dumping of project variables."""
        result = self._main(['variables', 'dump'])
        self.assertEqual(result, {
            'groups': {
                'i1/g1': {'key': 'value'}},
            'projects': {
                'i1/g1/p1': {'key': 'value', 'key2': 'value2'},
                'i2/g2/p2': {'key3': 'value3', 'key4': 'value4'}}})

    def test_generate(self):
        """Check the generation of project variables."""
        result = self._main(['variables', 'generate'])
        self.assertEqual(result, {
            'groups': {
                'i1/g1': {'key': 'value', 'key2': 'value2'}},
            'projects': {
                'i1/g1/p1': {'key': 'value', 'key2': 'value3'},
                'i2/g2/p2': {'key3': 'value3', }}})

    @responses.activate
    def test_diff(self):
        """Check the diffing of project variables."""
        changes = self._main(['variables', 'diff'])
        self.assertEqual(changes, [
            '--- current',
            '',
            '+++ proposed',
            '',
            '@@ -1,11 +1,11 @@',
            '',
            ' groups:',
            '   i1/g1:',
            '     key: value',
            '+    key2: value2',
            ' projects:',
            '   i1/g1/p1:',
            '     key: value',
            '-    key2: value2',
            '+    key2: value3',
            '   i2/g2/p2:',
            '     key3: value3',
            '-    key4: value4',
        ])

    @responses.activate
    def test_apply(self):
        """Check the application of project variables."""
        post_url = 'https://url1/api/v4/groups/g1/variables'
        put_url = 'https://url1/api/v4/projects/g1%2Fp1/variables/key2'
        delete_url = 'https://url2/api/v4/projects/g2%2Fp2/variables/key4'
        responses.add(responses.POST, post_url, json={})
        responses.add(responses.PUT, put_url, json={})
        responses.add(responses.DELETE, delete_url, json={})
        self._main(['variables', 'apply'])
        self.assertEqual(self._requests(method='POST'), [
            {'key': 'key2', 'value': 'value2', 'variable_type': 'env_var'}])
        self.assertEqual(self._requests(method='PUT'), [
            {'key': 'key2', 'value': 'value3', 'variable_type': 'env_var'}])
        self.assertEqual(self._requests(method='DELETE', parse=False), [None])


@mock.patch('cki_lib.misc.sentry_init', mock.Mock())
class TestRegistrations(TestRunnerConfig):
    """Tests the registrations functionality."""

    @responses.activate
    def test_dump(self):
        """Check the dumping of registrations."""
        result = self._main(['registrations', 'dump'])
        self.assertEqual(result, {
            'c1-d2-docker-high': {
                '.groups': ['i1/g1'],
                '.projects': [],
                'tag_list': ['t1'],
                'access_level': 'not_protected',
                'description': 'c1-d2-docker-high',
                'locked': True,
                'maximum_timeout': 86400,
                'run_untagged': False,
            },
            'c1-d2-docker-low': {
                '.groups': ['i1/g1'],
                '.projects': [],
                'tag_list': ['r2'],
                'access_level': 'not_protected',
                'description': 'c1-d2-docker-low',
                'locked': True,
                'maximum_timeout': 86400,
                'run_untagged': False,
            },
            'c1-d2-docker-priv': {
                '.groups': ['i1/g1'],
                '.projects': [],
                'tag_list': [],
                'access_level': 'not_protected',
                'description': 'c1-d2-docker-priv',
                'locked': True,
                'maximum_timeout': 86400,
                'run_untagged': False,
            },
            'c2-d2-docker-low': {
                '.groups': [],
                '.projects': ['i1/g1/p1', 'i1/g1/p3'],
                'tag_list': [],
                'access_level': 'not_protected',
                'description': 'c2-d2-docker-low',
                'locked': True,
                'maximum_timeout': 86400,
                'run_untagged': False,

            }
        })

    def test_generate(self):
        """Check the generation of registrations."""
        result = self._main(['registrations', 'generate'])
        self.assertEqual(result, {
            'c1-d1-kubernetes-low': {
                '.groups': ['i1/g1'],
                '.projects': [],
                'tag_list': ['r3'],
                'access_level': 'not_protected',
                'description': 'c1-d1-kubernetes-low',
                'locked': True,
                'maximum_timeout': 604800,
                'run_untagged': False,
            },
            'c1-d2-docker-high': {
                '.groups': ['i1/g1'],
                '.projects': [],
                'tag_list': [],
                'access_level': 'not_protected',
                'description': 'c1-d2-docker-high',
                'locked': True,
                'maximum_timeout': 604800,
                'run_untagged': False,
            },
            'c1-d2-docker-low': {
                '.groups': ['i1/g1'],
                '.projects': [],
                'tag_list': ['r1'],
                'access_level': 'not_protected',
                'description': 'c1-d2-docker-low',
                'locked': True,
                'maximum_timeout': 604800,
                'run_untagged': False,
            },
            'c1-d2-docker-priv': {
                '.groups': ['i1/g1'],
                '.projects': [],
                'tag_list': ['r2'],
                'access_level': 'not_protected',
                'description': 'c1-d2-docker-priv',
                'locked': True,
                'maximum_timeout': 604800,
                'run_untagged': False,
            },
            'c2-d2-docker-low': {
                '.groups': [],
                '.projects': ['i1/g1/p1', 'i1/g1/p2'],
                'tag_list': ['r1'],
                'access_level': 'not_protected',
                'description': 'c2-d2-docker-low',
                'locked': True,
                'maximum_timeout': 604800,
                'run_untagged': False,
            },
            'c2-d2-docker-priv': {
                '.groups': [],
                '.projects': ['i1/g1/p1', 'i1/g1/p2'],
                'tag_list': ['r2'],
                'access_level': 'not_protected',
                'description': 'c2-d2-docker-priv',
                'locked': True,
                'maximum_timeout': 604800,
                'run_untagged': False,
            },
        })

    @responses.activate
    def test_diff(self):
        """Check the diffing of registrations."""
        changes = self._main(['registrations', 'diff'])
        self.assertEqual(changes, [
            '--- current',
            '',
            '+++ proposed',
            '',
            '@@ -1,44 +1,68 @@',
            '',
            '+c1-d1-kubernetes-low:',
            '+  .groups:',
            '+  - i1/g1',
            '+  .projects: []',
            '+  access_level: not_protected',
            '+  description: c1-d1-kubernetes-low',
            '+  locked: true',
            '+  maximum_timeout: 604800',
            '+  run_untagged: false',
            '+  tag_list:',
            '+  - r3',
            ' c1-d2-docker-high:',
            '   .groups:',
            '   - i1/g1',
            '   .projects: []',
            '   access_level: not_protected',
            '   description: c1-d2-docker-high',
            '   locked: true',
            '-  maximum_timeout: 86400',
            '+  maximum_timeout: 604800',
            '   run_untagged: false',
            '-  tag_list:',
            '-  - t1',
            '+  tag_list: []',
            ' c1-d2-docker-low:',
            '   .groups:',
            '   - i1/g1',
            '   .projects: []',
            '   access_level: not_protected',
            '   description: c1-d2-docker-low',
            '   locked: true',
            '-  maximum_timeout: 86400',
            '+  maximum_timeout: 604800',
            '   run_untagged: false',
            '   tag_list:',
            '-  - r2',
            '+  - r1',
            ' c1-d2-docker-priv:',
            '   .groups:',
            '   - i1/g1',
            '   .projects: []',
            '   access_level: not_protected',
            '   description: c1-d2-docker-priv',
            '   locked: true',
            '-  maximum_timeout: 86400',
            '+  maximum_timeout: 604800',
            '   run_untagged: false',
            '-  tag_list: []',
            '+  tag_list:',
            '+  - r2',
            ' c2-d2-docker-low:',
            '   .groups: []',
            '   .projects:',
            '   - i1/g1/p1',
            '-  - i1/g1/p3',
            '+  - i1/g1/p2',
            '   access_level: not_protected',
            '   description: c2-d2-docker-low',
            '   locked: true',
            '-  maximum_timeout: 86400',
            '+  maximum_timeout: 604800',
            '   run_untagged: false',
            '-  tag_list: []',
            '+  tag_list:',
            '+  - r1',
            '+c2-d2-docker-priv:',
            '+  .groups: []',
            '+  .projects:',
            '+  - i1/g1/p1',
            '+  - i1/g1/p2',
            '+  access_level: not_protected',
            '+  description: c2-d2-docker-priv',
            '+  locked: true',
            '+  maximum_timeout: 604800',
            '+  run_untagged: false',
            '+  tag_list:',
            '+  - r2'
        ])

    @responses.activate
    def test_no_create_missing(self):
        """Check that --create-missing is needed."""
        self.assertRaises(Exception,
                          lambda: self._main(['registrations', 'apply']))

    @responses.activate
    def test_apply(self):
        """Check the application of registrations."""
        post_url1 = 'https://url1/api/v4/runners'
        post_url2 = 'https://url1/api/v4/projects/g1%2Fp2/runners'
        put_url1 = 'https://url1/api/v4/runners/1'
        put_url2 = 'https://url1/api/v4/runners/2'
        put_url3 = 'https://url1/api/v4/runners/3'
        put_url4 = 'https://url1/api/v4/runners/4'
        put_url5 = 'https://url1/api/v4/runners/6'
        delete_url1 = 'https://url1/api/v4/projects/g1%2Fp3/runners/4'
        responses.add(responses.POST, post_url1,
                      json={'id': 5, 'token': 'new-token-5'})
        responses.add(responses.POST, post_url1,
                      json={'id': 6, 'token': 'new-token-6'})
        responses.add(responses.POST, post_url2, json={})
        responses.add(responses.GET,
                      url='https://url1/api/v4/runners/5',
                      json={'id': 5, 'description': 'c1-d1-kubernetes-low',
                            'groups': [{'id': 1}],
                            'projects': [],
                            'tag_list': ['r3'],
                            'access_level': 'not_protected',
                            'active': True,
                            'locked': True,
                            'run_untagged': False,
                            'maximum_timeout': 7 * 24 * 60 * 60})
        responses.add(responses.GET,
                      url='https://url1/api/v4/runners/6',
                      json={'id': 6, 'description': 'c2-d2-docker-priv',
                            'groups': [],
                            'projects': [{'id': 1}],
                            'tag_list': ['r2'],
                            'access_level': 'not_protected',
                            'active': True,
                            'locked': True,
                            'run_untagged': False,
                            'maximum_timeout': 7 * 24 * 60 * 60})
        responses.add(responses.PUT, put_url1, json={})
        responses.add(responses.PUT, put_url2, json={})
        responses.add(responses.PUT, put_url3, json={})
        responses.add(responses.PUT, put_url4, json={})
        responses.add(responses.PUT, put_url5, json={})
        responses.add(responses.DELETE, delete_url1, json={})

        self._main(['registrations', 'apply', '--create-missing'])

        self.assertEqual(self._requests(method='POST', url=post_url1), [
            {'token': 'g1-token',
             'description': 'c1-d1-kubernetes-low',
             'tag_list': ['r3'],
             'access_level': 'not_protected',
             'active': True,
             'locked': True,
             'run_untagged': False,
             'maximum_timeout': 7 * 24 * 60 * 60},
            {'token': 'p1-token',
             'description': 'c2-d2-docker-priv',
             'tag_list': ['r2'],
             'access_level': 'not_protected',
             'active': True,
             'locked': True,
             'run_untagged': False,
             'maximum_timeout': 7 * 24 * 60 * 60}])
        self.assertEqual(self._requests(method='POST', url=post_url2), [
            {'runner_id': 4}, {'runner_id': 6}])
        self.assertEqual(self._requests(method='PUT', url=put_url1), [
            {'access_level': 'not_protected',
             'description': 'c1-d2-docker-high',
             'locked': True,
             'maximum_timeout': 604800,
             'run_untagged': False,
             'tag_list': []}])
        self.assertEqual(self._requests(method='PUT', url=put_url2), [
            {'access_level': 'not_protected',
             'description': 'c1-d2-docker-low',
             'locked': True,
             'maximum_timeout': 604800,
             'run_untagged': False,
             'tag_list': ['r1']}])
        self.assertEqual(self._requests(method='PUT', url=put_url3), [
            {'access_level': 'not_protected',
             'description': 'c1-d2-docker-priv',
             'locked': True,
             'maximum_timeout': 604800,
             'run_untagged': False,
             'tag_list': ['r2']}])
        self.assertEqual(self._requests(method='PUT', url=put_url4), [
            {'access_level': 'not_protected',
             'description': 'c2-d2-docker-low',
             'locked': True,
             'maximum_timeout': 604800,
             'run_untagged': False,
             'tag_list': ['r1']},
            {'locked': False}, {'locked': True}])
        self.assertEqual(self._requests(method='PUT', url=put_url5), [
            {'locked': False}, {'locked': True}])
        self.assertEqual(self._requests(url=delete_url1, parse=False), [None])


@mock.patch('cki_lib.misc.sentry_init', mock.Mock())
class TestActivations(TestRunnerConfig):
    """Tests the activations functionality."""

    @responses.activate
    def test_dump(self):
        """Check the dumping of activations."""
        result = self._main(['activations', 'dump'])
        self.assertEqual(result, {
            'c1-d2-docker-high': {'.online': True, 'active': False},
            'c1-d2-docker-low': {'.online': True, 'active': True},
            'c1-d2-docker-priv': {'.online': True, 'active': True},
            'c2-d2-docker-low': {'.online': True, 'active': True},
        })

    def test_generate(self):
        """Check the generation of activations."""
        result = self._main(['activations', 'generate'])
        self.assertEqual(result, {
            'c1-d1-kubernetes-low': {'.online': True, 'active': True},
            'c1-d2-docker-high': {'.online': True, 'active': True},
            'c1-d2-docker-low': {'.online': True, 'active': True},
            'c1-d2-docker-priv': {'.online': True, 'active': True},
            'c2-d2-docker-low': {'.online': True, 'active': True},
            'c2-d2-docker-priv': {'.online': True, 'active': True},
        })

    @responses.activate
    def test_diff(self):
        """Check the diffing of activations."""
        changes = self._main(['activations', 'diff'])
        self.assertEqual(changes, [
            '--- current',
            '',
            '+++ proposed',
            '',
            '@@ -1,13 +1,19 @@',
            '',
            '+c1-d1-kubernetes-low:',
            '+  .online: true',
            '+  active: true',
            ' c1-d2-docker-high:',
            '   .online: true',
            '-  active: false',
            '+  active: true',
            ' c1-d2-docker-low:',
            '   .online: true',
            '   active: true',
            ' c1-d2-docker-priv:',
            '   .online: true',
            '   active: true',
            ' c2-d2-docker-low:',
            '   .online: true',
            '   active: true',
            '+c2-d2-docker-priv:',
            '+  .online: true',
            '+  active: true',
        ])

    @responses.activate
    def test_apply(self):
        """Check the application of activations."""
        put_url1 = 'https://url1/api/v4/runners/1'
        responses.add(responses.PUT, put_url1, json={})

        self._main(['activations', 'apply'])

        self.assertEqual(self._requests(method='PUT', url=put_url1), [
            {'active': True},
        ])

    @responses.activate
    def test_apply_activate(self):
        """Check the application of activations."""
        put_url1 = 'https://url1/api/v4/runners/1'
        put_url4 = 'https://url1/api/v4/runners/4'
        responses.add(responses.PUT, put_url1, json={})
        responses.add(responses.PUT, put_url4, json={})

        self._main(['activations', 'apply', '--deactivate', 'c2-.*'])

        self.assertEqual(self._requests(method='PUT', url=put_url1), [
            {'active': True},
        ])
        self.assertEqual(self._requests(method='PUT', url=put_url4), [
            {'active': False},
        ])


@mock.patch('cki_lib.misc.sentry_init', mock.Mock())
class TestConfigurations(TestRunnerConfig):
    """Tests the configurations functionality."""

    d1_base64 = (
        'apiVersion: v1\n' +
        'kind: Secret\n' +
        'metadata:\n' +
        '  name: gitlab-runner\n' +
        'data:\n' +
        '  config.toml: ' +
        'W1tydW5uZXJzXV0KZW52aXJvbm1lbnQgPSBbICJrZXk9dmFsdWUiLCAia2V5Mj12YWx1ZTIiLF0KZXhlY3V0b3Ig'
        'PSAia3ViZXJuZXRlcyIKa2V5ID0gInZhbHVlIgpuYW1lID0gImMxLWQxLWt1YmVybmV0ZXMtbG93Igp0b2tlbiA9'
        'ICJydW5uZXItdG9rZW4tYzEtZDEta3ViZXJuZXRlcy1sb3ciCnVybCA9ICJodHRwczovL3VybDEiCgo=\n')
    d1_base64_value2 = (
        'apiVersion: v1\n' +
        'kind: Secret\n' +
        'metadata:\n' +
        '  name: gitlab-runner\n' +
        'data:\n' +
        '  config.toml: ' +
        'W1tydW5uZXJzXV0KZW52aXJvbm1lbnQgPSBbICJrZXk9dmFsdWUiLCAia2V5Mj12YWx1ZTIiLF0KZXhlY'
        '3V0b3IgPSAia3ViZXJuZXRlcyIKa2V5ID0gInZhbHVlMiIKbmFtZSA9ICJjMS1kMS1rdWJlcm5ldGVzLW'
        'xvdyIKdG9rZW4gPSAicnVubmVyLXRva2VuLWMxLWQxLWt1YmVybmV0ZXMtbG93Igp1cmwgPSAiaHR0cHM'
        '6Ly91cmwxIgoK\n')
    d1 = ('apiVersion: v1\n' +
          'kind: Secret\n' +
          'metadata:\n' +
          '  name: gitlab-runner\n' +
          'stringData:\n' +
          '  config.toml: |+\n' +
          '    [[runners]]\n' +
          '    environment = [ "key=value", "key2=value2",]\n' +
          '    executor = "kubernetes"\n' +
          '    key = "value"\n' +
          '    name = "c1-d1-kubernetes-low"\n' +
          '    token = "runner-token-c1-d1-kubernetes-low"\n' +
          '    url = "https://url1"\n' +
          '\n' +
          '...\n')
    d2 = ('[[runners]]\n' +
          'environment = [ "key=value", "key2=value2",]\n' +
          'executor = "docker"\n' +
          'name = "c1-d2-docker-high"\n' +
          'token = "runner-token-c1-d2-docker-high"\n' +
          'url = "https://url1"\n' +
          '\n' +
          '[[runners]]\n' +
          'environment = [ "key=value", "key2=value2",]\n' +
          'executor = "docker"\n' +
          'name = "c1-d2-docker-low"\n' +
          'token = "runner-token-c1-d2-docker-low"\n' +
          'url = "https://url1"\n' +
          '\n' +
          '[[runners]]\n' +
          'environment = [ "key=value", "key2=value2",]\n' +
          'executor = "docker"\n' +
          'name = "c1-d2-docker-priv"\n' +
          'token = "runner-token-c1-d2-docker-priv"\n' +
          'url = "https://url1"\n' +
          '\n' +
          '[[runners]]\n' +
          'executor = "docker"\n' +
          'name = "c2-d2-docker-low"\n' +
          'token = "runner-token-c2-d2-docker-low"\n' +
          'url = "https://url1"\n' +
          '\n' +
          '[[runners]]\n' +
          'executor = "docker"\n' +
          'name = "c2-d2-docker-priv"\n' +
          'token = "runner-token-c2-d2-docker-priv"\n' +
          'url = "https://url1"\n' +
          '\n')

    def test_dump(self):
        """Check the dumping of configurations."""
        with tempfile.TemporaryDirectory() as directory:
            pathlib.Path(directory, 'd1.yaml').write_text(self.d1_base64)
            pathlib.Path(directory, 'd2.toml').write_text(self.d2)
            result = self._main(['configurations', 'dump',
                                 '--directory', directory])
        self.assertEqual(result, {
            'd1.yaml': self.d1,
            'd2.toml': self.d2
        })

    def test_generate(self):
        """Check the generation of configurations."""
        result = self._main(['configurations', 'generate'])
        self.assertEqual(result, {
            'd1.yaml': self.d1,
            'd2.toml': self.d2
        })

    def test_diff(self):
        """Check the diffing of configurations."""
        with tempfile.TemporaryDirectory() as directory:
            pathlib.Path(directory, 'd1.yaml').write_text(self.d1_base64_value2)
            pathlib.Path(directory, 'd2.toml').write_text(self.d2)
            changes = self._main(['configurations', 'diff',
                                  '--directory', directory,
                                  '--context', '1'])
        self.assertEqual(changes, [
            '--- current',
            '',
            '+++ proposed',
            '',
            '@@ -10,3 +10,3 @@',
            '',
            '       executor = "kubernetes"',
            '-      key = "value2"',
            '+      key = "value"',
            '       name = "c1-d1-kubernetes-low"',
        ])

    def test_apply(self):
        """Check the application of configurations."""
        with tempfile.TemporaryDirectory() as directory:
            pathlib.Path(directory, 'd1.yaml').write_text(self.d1_base64_value2)
            pathlib.Path(directory, 'd2.toml').write_text(self.d2)
            self._main(['configurations', 'apply',
                        '--directory', directory])
            self.assertEqual(pathlib.Path(directory, 'd1.yaml').read_text(),
                             self.d1)
            self.assertEqual(pathlib.Path(directory, 'd2.toml').read_text(),
                             self.d2)


@mock.patch('cki_lib.misc.sentry_init', mock.Mock())
class TestDockerMachine(TestRunnerConfig):
    """Docker machine specifics."""

    config = {
        'runner_tokens': {
            'c1-d1-template1': 'runner-token1',
            'c1-d1-template2': 'runner-token2',
        },
        'gitlab_instances': {
            'i1': {'url': 'https://url1', 'api_token': 'token1'},
        },
        'runner_deployments': {
            'd1': {'.configfile': 'd1.toml'},
        },
        'runner_templates': {
            'template1': {'executor': 'docker+machine', 'machine': {
                '.MachineOptions': {
                    'amazonec2-use-private-address': True,
                    '.amazonec2-tags': {'a': 'b', 'c': 'd'}
                }
            }},
            'template2': {'executor': 'docker+machine', 'machine': {
                '.MachineOptions': {
                    'amazonec2-subnet-id': ['m', 'n'],
                    '.amazonec2-tags': {'e': 'f', 'g': 'h'}
                }
            }}
        },
        'runner_configurations': {
            'c1': {
                'runner_deployments': {'d1': ['template1', 'template2']},
                'gitlab_groups': 'i1/g1',
            },
        },
    }

    def test_apply(self):
        """Check the application of configurations."""
        with tempfile.TemporaryDirectory() as directory:
            self._main(['configurations', 'apply',
                        '--directory', directory])
            data = toml.loads(pathlib.Path(directory, 'd1.toml').read_text())
            self.assertEqual(data, {
                'runners': [{
                    'executor': 'docker+machine',
                    'name': 'c1-d1-template1',
                    'token': 'runner-token1',
                    'url': 'https://url1',
                    'machine': {
                        'MachineOptions': [
                            'amazonec2-tags=a,b,c,d',
                            'amazonec2-use-private-address=True',
                        ]
                    }
                }, {
                    'executor': 'docker+machine',
                    'name': 'c1-d1-template2',
                    'token': 'runner-token2',
                    'url': 'https://url1',
                    'machine': {
                        'MachineOptions': [
                            'amazonec2-subnet-id=m',
                            'amazonec2-subnet-id=n',
                            'amazonec2-tags=e,f,g,h'
                        ]
                    }
                }]
            })


@mock.patch('cki_lib.misc.sentry_init', mock.Mock())
class TestVariableOverrides(TestRunnerConfig):
    """Check the environment variable generation."""

    config = {
        'runner_tokens': {
            'c1-d1-template1': 'runner-token1',
            'c1-d1-template2': 'runner-token2',
            'c1-d1-template3': 'runner-token3',
            'c1-d2-template1': 'runner-token4',
            'c1-d2-template2': 'runner-token5',
            'c1-d2-template3': 'runner-token6',
        },
        'gitlab_instances': {
            'i1': {'url': 'https://url1', 'api_token': 'token1'},
        },
        'runner_deployments': {
            'd1': {'.configfile': 'd1.toml'},
            'd2': {'.configfile': 'd2.toml', '.template_overrides': {'.environment': {
                'key': 'value3', 'key4': 'value5',
            }}},
        },
        'runner_templates': {
            'template1': {},
            'template2': {'.environment': {'key2': 'value2'}},
            'template3': {'.environment': {'key4': 'value4'}},
        },
        'runner_configurations': {
            'c1': {
                'runner_deployments': {'d1': ['template1', 'template2', 'template3'],
                                       'd2': ['template1', 'template2', 'template3']},
                'variable_group': 'var1',
                'gitlab_groups': 'i1/g1',
            },
        },
        'variable_groups': {
            'var1': {'key': 'value'},
        },
    }

    def test_apply(self):
        """Check the application of configurations."""
        with tempfile.TemporaryDirectory() as directory:
            self._main(['configurations', 'apply',
                        '--directory', directory])
            self.assertEqual(toml.loads(pathlib.Path(directory, 'd1.toml').read_text()), {
                'runners': [{
                    'environment': ['key=value'],
                    'name': 'c1-d1-template1',
                    'token': 'runner-token1',
                    'url': 'https://url1',
                }, {
                    'environment': ['key=value', 'key2=value2'],
                    'name': 'c1-d1-template2',
                    'token': 'runner-token2',
                    'url': 'https://url1',
                }, {
                    'environment': ['key=value', 'key4=value4'],
                    'name': 'c1-d1-template3',
                    'token': 'runner-token3',
                    'url': 'https://url1',
                }]
            })
            self.assertEqual(toml.loads(pathlib.Path(directory, 'd2.toml').read_text()), {
                'runners': [{
                    'environment': ['key=value3', 'key4=value5'],
                    'name': 'c1-d2-template1',
                    'token': 'runner-token4',
                    'url': 'https://url1',
                }, {
                    'environment': ['key=value3', 'key2=value2', 'key4=value5'],
                    'name': 'c1-d2-template2',
                    'token': 'runner-token5',
                    'url': 'https://url1',
                }, {
                    'environment': ['key=value3', 'key4=value5'],
                    'name': 'c1-d2-template3',
                    'token': 'runner-token6',
                    'url': 'https://url1',
                }]
            })
