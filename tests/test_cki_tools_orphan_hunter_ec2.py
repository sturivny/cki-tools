"""Tests for cki.cki_tools.orphan_hunter_ec2."""
from datetime import timedelta
import typing
import unittest
from unittest import mock

from dateutil.parser import parse
import freezegun

from cki.cki_tools import orphan_hunter_ec2


class TestEc2Instance(unittest.TestCase):
    """Test Ec2Instance."""

    def test_is_untagged(self) -> None:
        """Test is_untagged works as expected."""
        tests: typing.List[typing.Tuple[typing.Any, bool]] = [
            ({}, True),
            ({'Tags': []}, True),
            ({'Tags': [{'Key': 'foo', 'Value': 'bar'}]}, False),
        ]
        for data, result in tests:
            self.assertEqual(orphan_hunter_ec2.Ec2Instance(data, None, None).is_untagged(),
                             result)

    def test_with_key(self) -> None:
        """Test with_key works as expected."""
        tests: typing.List[typing.Tuple[typing.Any, typing.List[str], bool]] = [
            ({}, [], False),
            ({}, ['foo'], False),
            ({'KeyName': 'foo'}, [], False),
            ({'KeyName': 'foo'}, ['bar'], False),
            ({'KeyName': 'foo'}, ['foo'], True),
            ({'KeyName': 'foo.bar'}, ['bar'], False),
            ({'KeyName': 'foo.bar'}, ['foo'], True),
        ]
        for data, prefix, result in tests:
            self.assertEqual(orphan_hunter_ec2.Ec2Instance(data, None, None).with_key(prefix),
                             result)

    def test_is_older(self) -> None:
        """Test is_older works as expected."""
        tests: typing.List[typing.Tuple[typing.Any, int, int, bool]] = [
            ({}, 0, 0, False),
            ({}, 0, 0, False),
            ({'LaunchTime': parse('2000-01-01T00:00:00Z')}, 0, 0, False),
            ({'LaunchTime': parse('2000-01-01T00:00:00Z')}, 0, 5, True),
            ({'LaunchTime': parse('2000-01-01T00:00:00Z')}, 5, 0, True),
            ({'LaunchTime': parse('2000-01-10T00:00:00Z')}, 0, 1, False),
            ({'LaunchTime': parse('2000-01-10T11:58:00Z')}, 5, 0, False),
        ]
        for data, hours, days, result in tests:
            with freezegun.freeze_time('2000-01-10T12:00:00Z'):
                self.assertEqual(orphan_hunter_ec2.Ec2Instance(data, None, None)
                                 .is_older(timedelta(hours=hours, days=days)), result)

    @staticmethod
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_terminate_production() -> None:
        """Test terminate works as expected in production mode."""
        ec2_client = mock.Mock()
        ec2_hunter = mock.Mock()
        orphan_hunter_ec2.Ec2Instance(
            {'InstanceId': 'i-1'}, ec2_client, ec2_hunter).terminate('foo')
        ec2_client.terminate_instances.assert_called_with(InstanceIds=['i-1'])

    @staticmethod
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_terminate_production_messagequeue() -> None:
        """Test terminate works as expected in production mode."""
        ec2_client = mock.Mock()
        ec2_hunter = mock.Mock()
        orphan_hunter_ec2.Ec2Instance(
            {'InstanceId': 'i-1'}, ec2_client, ec2_hunter).terminate('foo')
        ec2_client.terminate_instances.assert_called_with(InstanceIds=['i-1'])
        ec2_hunter.log_message.assert_called()

    @staticmethod
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_terminate_non_production() -> None:
        """Test terminate works as expected in non-production mode."""
        ec2_client = mock.Mock()
        ec2_hunter = mock.Mock()
        orphan_hunter_ec2.Ec2Instance(
            {'InstanceId': 'i-1'}, ec2_client, ec2_hunter).terminate('foo')
        ec2_client.terminate_instances.assert_not_called()
        ec2_hunter.log_message.assert_not_called()


class TestEc2SpotInstanceRequest(unittest.TestCase):
    """Test Ec2SpotInstanceRequest."""

    def test_with_key(self) -> None:
        """Test with_key works as expected."""
        tests: typing.List[typing.Tuple[typing.Any, typing.List[str], bool]] = [
            ({}, [], False),
            ({}, ['foo'], False),
            ({'LaunchSpecification': {}}, [], False),
            ({'LaunchSpecification': {}}, ['foo'], False),
            ({'LaunchSpecification': {'KeyName': 'foo'}}, [], False),
            ({'LaunchSpecification': {'KeyName': 'foo'}}, ['bar'], False),
            ({'LaunchSpecification': {'KeyName': 'foo'}}, ['foo'], True),
            ({'LaunchSpecification': {'KeyName': 'foo.bar'}}, ['bar'], False),
            ({'LaunchSpecification': {'KeyName': 'foo.bar'}}, ['foo'], True),
        ]
        for data, prefix, result in tests:
            self.assertEqual(orphan_hunter_ec2.Ec2SpotInstanceRequest(
                data, None, None).with_key(prefix), result)

    def test_is_older(self) -> None:
        """Test is_older works as expected."""
        tests: typing.List[typing.Tuple[typing.Any, int, int, bool]] = [
            ({}, 0, 0, False),
            ({}, 0, 0, False),
            ({'CreateTime': parse('2000-01-01T00:00:00Z')}, 0, 0, False),
            ({'CreateTime': parse('2000-01-01T00:00:00Z')}, 0, 5, True),
            ({'CreateTime': parse('2000-01-01T00:00:00Z')}, 5, 0, True),
            ({'CreateTime': parse('2000-01-10T00:00:00Z')}, 0, 1, False),
            ({'CreateTime': parse('2000-01-10T11:58:00Z')}, 5, 0, False),
        ]
        for data, hours, days, result in tests:
            with freezegun.freeze_time('2000-01-10T12:00:00Z'):
                self.assertEqual(orphan_hunter_ec2.Ec2SpotInstanceRequest(data, None, None)
                                 .is_older(timedelta(hours=hours, days=days)), result)

    @staticmethod
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_cancel_production() -> None:
        """Test cancel works as expected in production mode."""
        ec2_client = mock.Mock()
        ec2_hunter = mock.Mock()
        orphan_hunter_ec2.Ec2SpotInstanceRequest({
            'SpotInstanceRequestId': 'sir-1',
            'CreateTime': parse('2000-01-10T11:58:00Z'),
        }, ec2_client, ec2_hunter).cancel('foo')
        ec2_client.cancel_spot_instance_requests.assert_called_with(
            SpotInstanceRequestIds=['sir-1'])

    @staticmethod
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_cancel_production_messagequeue() -> None:
        """Test cancel works as expected in production mode."""
        ec2_client = mock.Mock()
        ec2_hunter = mock.Mock()
        orphan_hunter_ec2.Ec2SpotInstanceRequest({
            'SpotInstanceRequestId': 'sir-1',
            'CreateTime': parse('2000-01-10T11:58:00Z'),
        }, ec2_client, ec2_hunter).cancel('foo')
        ec2_client.cancel_spot_instance_requests.assert_called_with(
            SpotInstanceRequestIds=['sir-1'])
        ec2_hunter.log_message.assert_called()

    @staticmethod
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_cancel_non_production() -> None:
        """Test cancel works as expected in non-production mode."""
        ec2_client = mock.Mock()
        ec2_hunter = mock.Mock()
        orphan_hunter_ec2.Ec2SpotInstanceRequest({
            'SpotInstanceRequestId': 'sir-1',
            'CreateTime': parse('2000-01-10T11:58:00Z'),
        }, ec2_client, ec2_hunter).cancel('foo')
        ec2_client.cancel_spot_instance_requests.assert_not_called()
        ec2_hunter.log_message.assert_not_called()


class TestEc2Hunter(unittest.TestCase):
    """Test Ec2Hunter."""

    @mock.patch('cki.cki_tools.orphan_hunter_ec2.Session')
    def test_instances(self, session: typing.Any) -> None:
        """Test terminate works as expected in non-production mode."""
        session().client().describe_instances.return_value = {'Reservations': [
            {'Instances': [
                {'InstanceId': 'i-4', 'State': {'Name': 'running'}},
            ]},
            {'Instances': [
                {'InstanceId': 'i-5', 'State': {'Name': 'stopped'}},
            ]},
        ]}
        hunter = orphan_hunter_ec2.Ec2Hunter(None)
        self.assertEqual([i.data['InstanceId'] for i in hunter.instances()],
                         ['i-4', 'i-5'])

    @mock.patch('cki.cki_tools.orphan_hunter_ec2.Session', mock.Mock())
    @mock.patch('cki.cki_tools.orphan_hunter_ec2.MessageQueue')
    def test_send_message(self, messagequeue) -> None:
        """Test send_message works as expected."""
        with orphan_hunter_ec2.Ec2Hunter.summarize('exchange') as ec2_hunter:
            ec2_hunter.log_message('resource', 'foo')
        messagequeue().send_message.assert_called_with(mock.ANY, 'irc.message', 'exchange')

    @mock.patch('cki.cki_tools.orphan_hunter_ec2.Session', mock.Mock())
    @mock.patch('cki.cki_tools.orphan_hunter_ec2.MessageQueue')
    def test_send_no_message(self, messagequeue) -> None:
        """Test send_message works without an exchange."""
        with orphan_hunter_ec2.Ec2Hunter.summarize(None) as ec2_hunter:
            ec2_hunter.log_message('resource', 'foo')
        messagequeue().send_message.assert_not_called()

    @mock.patch('cki.cki_tools.orphan_hunter_ec2.Session', mock.Mock())
    @mock.patch('cki.cki_tools.orphan_hunter_ec2.MessageQueue')
    def test_send_summary_message(self, messagequeue) -> None:
        """Test send_message works as expected."""
        with orphan_hunter_ec2.Ec2Hunter.summarize('exchange', summary_only=True) as ec2_hunter:
            ec2_hunter.log_message('resource', 'foo')
            messagequeue().send_message.assert_not_called()
        messagequeue().send_message.assert_called_with(mock.ANY, 'irc.message', 'exchange')


class TestMain(unittest.TestCase):
    """Test main method."""

    @mock.patch('cki.cki_tools.orphan_hunter_ec2.Session')
    def test_main(self, session: typing.Any) -> None:
        """Test terminate works as expected in non-production mode."""
        session().client().describe_instances.return_value = {'Reservations': [{'Instances': [{
            'InstanceId': 'i-1',
            'KeyName': 'untagged.bar',
            'State': {'Name': 'running'},
            'LaunchTime': parse('2000-01-01T00:00:00Z'),
        }, {
            'InstanceId': 'i-untagged-tags',
            'KeyName': 'untagged.bar',
            'State': {'Name': 'running'},
            'LaunchTime': parse('2000-01-01T00:00:00Z'),
            'Tags': [{'Key': 'foo', 'Value': 'bar'}],
        }, {
            'InstanceId': 'i-untagged-too-new',
            'KeyName': 'untagged.bar',
            'State': {'Name': 'running'},
            'LaunchTime': parse('2000-01-10T11:58:00Z'),
        }, {
            'InstanceId': 'i-untagged-wrong-key',
            'KeyName': 'bar.foo',
            'State': {'Name': 'running'},
            'LaunchTime': parse('2000-01-01T00:00:00Z'),
        }, {
            'InstanceId': 'i-2',
            'KeyName': 'runaway.bar',
            'State': {'Name': 'running'},
            'LaunchTime': parse('2000-01-01T00:00:00Z'),
            'Tags': [{'Key': 'foo', 'Value': 'bar'}],
        }, {
            'InstanceId': 'i-runaway-too-new',
            'KeyName': 'runaway.bar',
            'State': {'Name': 'running'},
            'LaunchTime': parse('2000-01-10T00:00:00Z'),
            'Tags': [{'Key': 'foo', 'Value': 'bar'}],
        }, {
            'InstanceId': 'i-runaway-wrong-key',
            'KeyName': 'foo.bar',
            'State': {'Name': 'running'},
            'LaunchTime': parse('2000-01-01T00:00:00Z'),
            'Tags': [{'Key': 'foo', 'Value': 'bar'}],
        }]}]}
        session().client().describe_spot_instance_requests.return_value = {
            'SpotInstanceRequests': [{
                'SpotInstanceRequestId': 'sir-1',
                'LaunchSpecification': {'KeyName': 'spot.bar'},
                'CreateTime': parse('2000-01-01T00:00:00Z'),
            }, {
                'SpotInstanceRequestId': 'sir-spot-too-new',
                'LaunchSpecification': {'KeyName': 'spot.bar'},
                'CreateTime': parse('2000-01-10T11:58:00Z'),
            }, {
                'SpotInstanceRequestId': 'sir-spot-wrong-key',
                'LaunchSpecification': {'KeyName': 'foo.bar'},
                'CreateTime': parse('2000-01-01T00:00:00Z'),
            }],
        }
        instances = []
        requests = []
        with freezegun.freeze_time('2000-01-10T12:00:00Z'), \
            mock.patch.object(orphan_hunter_ec2.Ec2Instance,
                              'terminate', autospec=True) as terminate, \
            mock.patch.object(orphan_hunter_ec2.Ec2SpotInstanceRequest,
                              'cancel', autospec=True) as cancel:
            terminate.side_effect = lambda self, reason: instances.append(self.data['InstanceId'])
            cancel.side_effect = lambda self, reason: requests.append(
                self.data['SpotInstanceRequestId'])
            orphan_hunter_ec2.main(['--untagged-key-prefix', 'untagged.',
                                    '--untagged-age', '1h',
                                    '--runaway-key-prefix', 'runaway.',
                                    '--runaway-age', '7d',
                                    '--spot-key-prefix', 'spot.',
                                    '--spot-age', '1h'])
        self.assertEqual(instances, ['i-1', 'i-2'])
        self.assertEqual(requests, ['sir-1'])
