"""Beaker metrics via TEIID."""
from collections import defaultdict
import itertools
import os
import re

from cki_lib.cronjob import CronJob
from cki_lib.psql import PSQLHandler
import prometheus_client
import yaml

BEAKER_CONFIG = yaml.safe_load(os.environ.get('BEAKER_CONFIG', ''))
ALL_STATUSES = {
    'Aborted', 'Cancelled', 'Completed', 'Installing', 'New',
    'Processed', 'Queued', 'Running', 'Scheduled', 'Waiting',
}
ALL_PRIORITIES = {'Low', 'Medium', 'Normal', 'High', 'Urgent'}


class TEIIDBeakerMetrics(CronJob):
    """Calculate Beaker metrics from TEIID."""

    schedule = '*/5 * * * *'

    metric_recipes_in_queue = prometheus_client.Gauge(
        'cki_beaker_recipes_in_queue',
        'Number of jobs in a Beaker queue',
        ['queue']
    )

    metric_recipes_by_status = prometheus_client.Gauge(
        'cki_beaker_recipes_by_status',
        'Number of recipes per status',
        ['status', 'priority']
    )

    metric_recipes_queue_time = prometheus_client.Histogram(
        'cki_beaker_recipes_queue_time',
        'Queue time for all currently queued recipes in seconds',
        buckets=[3600 * h for h in [0.5, 1, 3, 6, 12, 24, 48]],
    )

    def __init__(self):
        """Initialize."""
        super().__init__()
        self.db_handler = PSQLHandler(
            os.environ.get('TEIID_HOST', 'localhost'),
            os.environ.get('TEIID_PORT', '5433'),
            os.environ.get('TEIID_DATABASE', 'public'),
            require_ssl=True
        )

    def get_queued_recipes_constraints(self):
        """Get distro_requires for the queued recipes."""
        query = """
            SELECT
                "r._distro_requires"
            FROM
                beaker.recipe AS r
            JOIN
                beaker.recipe_set AS rs ON rs.id = r.recipe_set_id
            JOIN
                beaker.job AS j ON j.id = rs.job_id
            JOIN
                beaker.tg_user AS u ON u.user_id = j.owner_id
            WHERE
                r.status = 'Queued'
            AND
                u.user_name = 'beaker/cki-team-automation';
        """
        return [r[0] for r in self.db_handler.execute(query)]

    def get_recipes_by_status(self):
        """Get recipes grouped by status."""
        query = """
            SELECT
                r.status,
                rs.priority,
                count(r.status)
            FROM
                beaker.recipe AS r
            JOIN
                beaker.recipe_set AS rs ON rs.id = r.recipe_set_id
            JOIN
                beaker.job AS j ON j.id = rs.job_id
            JOIN
                beaker.tg_user AS u ON u.user_id = j.owner_id
            WHERE
                u.user_name = 'beaker/cki-team-automation'
            GROUP BY
                r.status, rs.priority;
        """
        return [(r[0], r[1], int(r[2])) for r in self.db_handler.execute(query)]

    @staticmethod
    def _recipe_matches_queue(distro_requires, architectures):
        """Check if the distro_requires matches all the specified contraints."""
        required_architectures = re.findall(r'distro_arch op="=" value="(\S+)"', distro_requires)
        return set(required_architectures) == set(architectures)

    def update_cki_beaker_recipes_in_queue(self):
        """Update cki_beaker_recipes_in_queue metric."""
        queues = defaultdict(int)
        queued_recipes = self.get_queued_recipes_constraints()

        for queue_name, architectures in BEAKER_CONFIG.get('queues', {}).items():
            architectures = architectures or [queue_name]
            for recipe_distro_required in queued_recipes:
                if self._recipe_matches_queue(recipe_distro_required, architectures):
                    queues[queue_name] += 1

            self.metric_recipes_in_queue.labels(queue_name).set(queues[queue_name])

    def update_cki_beaker_recipes_by_status(self):
        """Update cki_beaker_recipes_by_status metric."""
        recipes_by_status = self.get_recipes_by_status()

        for status_name, priority_name, count in recipes_by_status:
            self.metric_recipes_by_status.labels(status_name, priority_name).set(count)

        # Set all labels not returned by get_recipes_by_status to 0.
        all_labels = set(itertools.product(ALL_STATUSES, ALL_PRIORITIES))
        set_labels = {(recipe[0], recipe[1]) for recipe in recipes_by_status}
        # sorted() for reproducible testing.
        for status_name, priority_name in sorted(all_labels - set_labels):
            self.metric_recipes_by_status.labels(status_name, priority_name).set(0)

    def get_recipes_queued_times(self):
        """Get recipes queued time."""
        query = """
            SELECT
               rs.queue_time,
               now()
            FROM
                beaker.recipe AS r
            JOIN
                beaker.recipe_set AS rs ON rs.id = r.recipe_set_id
            JOIN
                beaker.job AS j ON j.id = rs.job_id
            JOIN
                beaker.tg_user AS u ON u.user_id = j.owner_id
            WHERE
                u.user_name = 'beaker/cki-team-automation'
            AND
                r.status = 'Queued';
        """
        return [
            # Calculate time difference as it's not supported in the version of TEIID we run
            r[1] - r[0] for r in self.db_handler.execute(query)
        ]

    def update_cki_beaker_recipes_queue_time(self):
        """Update cki_beaker_recipes_queue_time metric."""
        queue_times = self.get_recipes_queued_times()
        for queue_time in queue_times:
            self.metric_recipes_queue_time.observe(queue_time.total_seconds())

    def run(self, **_):
        """Update all metrics."""
        self.update_cki_beaker_recipes_in_queue()
        self.update_cki_beaker_recipes_by_status()
        self.update_cki_beaker_recipes_queue_time()
