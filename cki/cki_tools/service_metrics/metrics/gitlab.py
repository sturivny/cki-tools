"""Export information about GitLab."""
from collections import defaultdict
import os
import typing
from urllib import parse

from cki_lib import gitlab
from cki_lib import misc
from cki_lib.cronjob import CronJob
from cki_lib.logger import get_logger
import prometheus_client
import yaml

GITLAB_CONFIG = yaml.safe_load(os.environ.get('GITLAB_CONFIG', ''))
KNOWN_PLANS = ['free', 'premium', 'ultimate', 'opensource']
KNOWN_ALERT_STATUSES = ['executable', 'disabled', 'temporarily_disabled']
LOGGER = get_logger(__name__)


class GitLabMetricsHourly(CronJob):
    """Calculate GitLab metrics."""

    schedule = '0 * * * *'  # once per hour

    metric_namespace_plan = prometheus_client.Enum(
        'cki_gitlab_namespace_plan',
        'Paid plan for a namespace',
        ['instance', 'namespace'],
        states=KNOWN_PLANS + ['unknown'],
    )

    metric_namespace_size = prometheus_client.Gauge(
        'cki_gitlab_namespace_size',
        'Size statistics for a namespace',
        ['instance', 'namespace', 'statistic'],
    )

    metric_project_size = prometheus_client.Gauge(
        'cki_gitlab_project_size',
        'Size statistics for a project',
        ['instance', 'project', 'archived', 'statistic'],
    )

    size_query = '''
      query storage_statistics(
        $fullPath: ID!
        $after: String = ""
      ) {
        namespace(fullPath: $fullPath) {
          fullPath
          rootStorageStatistics {
            buildArtifactsSize
            containerRegistrySize
            lfsObjectsSize
            packagesSize
            pipelineArtifactsSize
            repositorySize
            snippetsSize
            storageSize
            uploadsSize
            wikiSize
          }
          projects(
            includeSubgroups: true
            after: $after
          ) {
            nodes {
              fullPath
              archived
              statistics {
                buildArtifactsSize
                containerRegistrySize
                lfsObjectsSize
                packagesSize
                pipelineArtifactsSize
                repositorySize
                snippetsSize
                storageSize
                uploadsSize
                wikiSize
              }
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }
      }
    '''

    def update_metric_namespace_plan(self) -> None:
        """Update the namespace plan metric."""
        for namespace_url in GITLAB_CONFIG.get('namespaces', []):
            url_parts = parse.urlsplit(namespace_url)

            instance_url = parse.urlunparse(url_parts[:2] + ('',) * 4)
            gl_instance = gitlab.get_instance(instance_url)
            gl_namespace = gl_instance.namespaces.get(url_parts.path[1:])

            plan = gl_namespace.plan if gl_namespace.plan in KNOWN_PLANS else 'unknown'
            # pylint: disable=no-member
            self.metric_namespace_plan.labels(gl_instance.url, gl_namespace.full_path).state(plan)

    def update_metric_namespace_size(self) -> None:
        """Update the namespace size metrics."""
        for namespace_url in GITLAB_CONFIG.get('namespaces', []):
            url_parts = parse.urlsplit(namespace_url)

            instance_url = parse.urlunparse(url_parts[:2] + ('',) * 4)
            gl_client = gitlab.get_graphql_client(instance_url)
            result = gl_client.query(self.size_query,
                                     variable_values={'fullPath': url_parts.path[1:]},
                                     paged_key='namespace/projects')
            for key, value in (result['namespace']['rootStorageStatistics'] or {}).items():
                self.metric_namespace_size.labels(
                    instance_url, result['namespace']['fullPath'], key).set(value)
            for project in result['namespace']['projects']['nodes']:
                for key, value in project['statistics'].items():
                    self.metric_project_size.labels(
                        instance_url, project['fullPath'], misc.booltostr(project['archived']), key
                    ).set(value)

    def run(self, **_: typing.Any) -> None:
        """Update the metrics."""
        self.update_metric_namespace_plan()
        self.update_metric_namespace_size()


class GitLabMetricsMinutely(CronJob):
    """Calculate GitLab metrics."""

    schedule = '*/2 * * * *'  # every 2 minutes

    metric_jobs_pending = prometheus_client.Gauge(
        'cki_gitlab_jobs_pending', 'Number of jobs in pending state',
        ['name', 'stage'],
    )

    metric_jobs_queued_time = prometheus_client.Histogram(
        'cki_gitlab_jobs_queued_time', 'How long jobs are queued before starting',
        ['name', 'stage'],
        buckets=[60 * m for m in [1, 5, 10, 30, 60, 600]],
    )

    metric_webhook_alert_status = prometheus_client.Enum(
        'cki_gitlab_webhook_alert_status',
        'Alert status of webhooks',
        ['instance', 'path', 'url'],
        states=KNOWN_ALERT_STATUSES + ['unknown'],
    )

    def update_metric_pending_jobs(self):
        """Update the pending jobs metrics."""
        pending_jobs_count = defaultdict(int)
        # export zeros for known jobs where pending=0
        for stage, jobs in GITLAB_CONFIG.get('jobs', {}).items():
            for job in jobs:
                pending_jobs_count[(job, stage)] = 0
        for project_url in GITLAB_CONFIG.get('projects', []):
            _, gl_project = gitlab.parse_gitlab_url(project_url)
            for job in gl_project.jobs.list(scope='pending', iterator=True):
                pending_jobs_count[(job.name, job.stage)] += 1
                self.metric_jobs_queued_time.labels(name=job.name,
                                                    stage=job.stage).observe(job.queued_duration)

        for (name, stage), counter in pending_jobs_count.items():
            self.metric_jobs_pending.labels(name=name,
                                            stage=stage).set(counter)

    def update_metric_webhook_alert_status(self) -> None:
        """Update the webhook alert status metric."""
        groups = set()
        projects = set(GITLAB_CONFIG.get('projects', []))
        for project in projects:
            url_parts = parse.urlsplit(project)
            project_parts = url_parts.path[1:].split('/')
            groups.update(
                parse.urlunsplit(url_parts._replace(path='/'.join(project_parts[:i])))
                for i in range(1, len(project_parts))
            )
        self._update_metric_webhook_alert_status('groups', groups)
        self._update_metric_webhook_alert_status('projects', projects)

    def _update_metric_webhook_alert_status(self, what, urls) -> None:
        for url in urls:
            url_parts = parse.urlsplit(url)

            instance_url = parse.urlunparse(url_parts[:2] + ('',) * 4)
            gl_instance = gitlab.get_instance(instance_url)
            gl_object = getattr(gl_instance, what).get(url_parts.path[1:], lazy=True)
            try:
                gl_hooks = gl_object.hooks.list(all=True)
            except Exception:  # pylint: disable=broad-except
                gl_hooks = []
            for gl_hook in gl_hooks:
                alert_status = gl_hook.attributes.get('alert_status', 'unknown')
                # pylint: disable=no-member
                self.metric_webhook_alert_status.labels(
                    instance_url, url_parts.path[1:], gl_hook.url,
                ).state(
                    alert_status if alert_status in KNOWN_ALERT_STATUSES else 'unknown'
                )

    def run(self, **_):
        """Update the metrics."""
        self.update_metric_pending_jobs()
        self.update_metric_webhook_alert_status()
