"""Trigger pipelines from brew/koji."""
import argparse
import os
import pathlib

from cki_lib import brew
from cki_lib import cki_pipeline
from cki_lib import logger
from cki_lib import misc
from cki_lib import session
from cki_lib import yaml
from gitlab import Gitlab
import sentry_sdk

LOGGER = logger.get_logger('cki.cki_tools.brew_trigger')
SESSION = session.get_session('cki.cki_tools.brew_trigger')


def main():
    """Command line interface to the triggering brew pipelines."""
    misc.sentry_init(sentry_sdk)
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--gitlab-url', default=os.environ.get('GITLAB_URL'),
        help='GitLab URL (default: value of GITLAB_URL environment variable)')
    parser.add_argument(
        '--private-token', default='GITLAB_PRIVATE_TOKEN',
        help='Name of the env variable that contains the GitLab private token'
        ' (default: GITLAB_PRIVATE_TOKEN)')
    config = parser.add_mutually_exclusive_group()
    config.add_argument(
        '-c', '--config', default='brew.yaml',
        help='YAML configuration file to use (default: brew.yaml)')
    config.add_argument(
        '--config-url',
        help='URL to YAML configuration file to use.')
    parser.add_argument(
        '--server-section', default='.amqp',
        help='YAML section with server configuration (default: .amqp)')
    parser.add_argument(
        "--variables", action=misc.StoreNameValuePair, default={},
        metavar='KEY=VALUE', help='Override trigger variables')
    parser.add_argument(
        'task_id',
        help='Brew task ID')
    args = parser.parse_args()

    if args.config_url:
        config_content = SESSION.get(args.config_url).content
    else:
        config_content = pathlib.Path(args.config).read_text(encoding='utf8')

    config = yaml.load(contents=config_content, resolve_references=True)
    variables = brew.get_brew_trigger_variables(args.task_id, config,
                                                args.server_section)
    if not args.gitlab_url:
        raise Exception(
            'GitLab instance URL required via --gitlab-url/GITLAB_URL')
    gl_instance = Gitlab(args.gitlab_url,
                         private_token=os.getenv(args.private_token),
                         session=SESSION)
    gl_project = cki_pipeline.pipeline_project(gl_instance, variables)
    gl_pipeline = cki_pipeline.trigger(gl_project, variables,
                                       variable_overrides=args.variables,
                                       interactive=True,
                                       non_production_delay_s=0)  # interactive use
    print(f'Pipeline: {gl_pipeline.web_url}')


if __name__ == '__main__':
    main()
