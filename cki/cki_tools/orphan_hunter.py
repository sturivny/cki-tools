"""Delete PODs orphaned by gitlab-runner."""
import datetime
import os

from cki_lib import misc
from cki_lib.gitlab import get_instance
from cki_lib.kubernetes import KubernetesHelper
from cki_lib.session import get_session
import dateutil
import gitlab
import sentry_sdk

from cki.cki_tools import _utils

SESSION = get_session('cki.cki_tools.orphan_hunter')


class GitLabRunnerPod:
    """Manage a pod spawned by gitlab-runner."""

    def __init__(self, api, pod):
        """Initialize the pod and parse environment variables."""
        self._api = api
        self._pod = pod
        self.name = self._pod.metadata.name

        env = {x.name: x.value
               for x in self._pod.spec.containers[0].env}
        self._gitlab = get_instance(f'https://{env["CI_SERVER_HOST"]}')
        self.project_path = env['CI_PROJECT_PATH']
        self._job_id = env['CI_JOB_ID']
        self.job_url = env['CI_JOB_URL']

    def job_status(self):
        """Check whether the GitLab job is still running."""
        job = self._get_job()
        if not job:
            # project exists, but job not? try again just to be sure
            job = self._get_job()
        if not job:
            return 'non-existing'
        if not job.finished_at:
            return 'not finished'
        now = datetime.datetime.now(datetime.timezone.utc)
        finished = dateutil.parser.parse(job.finished_at)
        # wait 5 minutes for the runner to clean up after itself
        if now - finished < datetime.timedelta(minutes=5):
            return 'not finished'
        return 'finished'

    def delete(self):
        """Delete the pod."""
        self._api.delete_namespaced_pod(name=self.name,
                                        namespace=self._pod.metadata.namespace,
                                        grace_period_seconds=0)

    def _get_job(self):
        """If the project exists, return the job or None; otherwise, throw."""
        project = self._gitlab.projects.get(self.project_path)
        try:
            return project.jobs.get(self._job_id)
        except gitlab.exceptions.GitlabGetError:
            return None


def notify_pod(pod, status, delete):
    """Send a message to the IRC bot."""
    if 'IRCBOT_URL' not in os.environ:
        return
    action = 'Deleting' if delete else 'Detected'
    url = misc.shorten_url(pod.job_url)
    message = (f'👻 {action} abandoned pod {pod.name} from {status} job '
               f'of {pod.project_path} {url}')
    SESSION.post(os.environ['IRCBOT_URL'], json={'message': message})


def main():
    """Delete PODs orphaned by gitlab-runner."""
    misc.sentry_init(sentry_sdk)

    _utils.cprint(_utils.Colors.YELLOW, 'Loading configuration...')
    k8s_helper = KubernetesHelper()
    k8s_helper.setup()
    api = k8s_helper.api_corev1()
    print(f'  using {k8s_helper.config_type}')

    _utils.cprint(_utils.Colors.YELLOW, 'Getting pods...')
    all_pods = api.list_namespaced_pod(k8s_helper.namespace).items
    runner_pods = [p for p in all_pods
                   if p.metadata.name.startswith('runner-')]
    print(f'  Found {len(runner_pods)} gitlab-runner pods ' +
          f'out of {len(all_pods)} pods in total')

    for runner_pod in runner_pods:
        with misc.only_log_exceptions():
            pod = GitLabRunnerPod(api, runner_pod)
            _utils.cprint(_utils.Colors.YELLOW, f'Checking {pod.name}...')
            print(f'  {pod.job_url} ', end='')
            status = pod.job_status()
            if status == 'not finished':
                _utils.cprint(_utils.Colors.GREEN, status)
            else:
                _utils.cprint(_utils.Colors.RED, status)
                delete = os.environ.get('ACTION', 'report') == 'delete'
                notify_pod(pod, status, delete)
                if delete:
                    print('  Deleting pod... ')
                    pod.delete()


if __name__ == '__main__':
    main()
