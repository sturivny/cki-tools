"""Spawn a cronjob to keep krb ticket valid."""
import subprocess

from cki_lib import cronjob
from cki_lib import misc
import sentry_sdk


class RefreshKerberosTicket(cronjob.CronJob):
    """Issue a new krb ticket every hour."""

    schedule = '0 * * * *'

    def run(self, **_):
        """Run job."""
        subprocess.run(['/usr/bin/kinit', '-R'], check=True)


if __name__ == '__main__':
    misc.sentry_init(sentry_sdk)
    cronjob.run([RefreshKerberosTicket()])
