#!/usr/bin/python3
"""Gitlab webhooks sentinel."""
from cki_lib import metrics
from cki_lib import misc
import sentry_sdk

from cki.cki_tools import webhook_sentinel

if __name__ == '__main__':
    misc.sentry_init(sentry_sdk)
    metrics.prometheus_init()

    webhook_sentinel.main()
