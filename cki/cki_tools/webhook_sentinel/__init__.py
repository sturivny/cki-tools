"""Gitlab webhooks sentinel."""
import os

from cki_lib import messagequeue
from cki_lib import misc
from cki_lib.gitlab import get_instance
from cki_lib.logger import get_logger
import prometheus_client

GITLAB = get_instance('https://gitlab.com')
IS_PRODUCTION = misc.is_production()
LOGGER = get_logger(__name__)

RABBITMQ_QUEUE = os.environ.get('RABBITMQ_QUEUE')
RABBITMQ_EXCHANGE_SRC = os.environ.get('RABBITMQ_EXCHANGE_SRC')
RABBITMQ_EXCHANGE_DST = os.environ.get('RABBITMQ_EXCHANGE_DST')

METRIC_STALE_WEBHOOK_COUNT = prometheus_client.Counter(
    'cki_sentinel_stale_webhook',
    'Number of stale events detected',
    ['status']
)

METRIC_JOB_DURATION = prometheus_client.Summary(
    'cki_sentinel_job_duration',
    'Value of the duration field when the webhook is received',
    ['status', 'stage']
)

QUEUE = messagequeue.MessageQueue()


def update_stale_webhook(body):
    """Update information from outdated webhook."""
    job = GITLAB.projects.get(body['project_id'], lazy=True).jobs.get(body['build_id'])
    is_stale = job.status != body['build_status']

    LOGGER.warning(' - API data for job_id %d: status: "%s", duration %s - statuses are %s',
                   body['build_id'], job.status, job.duration,
                   'different' if is_stale else 'equal')

    if not is_stale:
        return None

    METRIC_STALE_WEBHOOK_COUNT.labels(body['build_status']).inc()

    updated_fields = {
        **body,
        'build_status': job.status,
    }

    if job.status in ('success', 'failed', 'canceled'):
        updated_fields.update({
            'build_duration': job.duration,
            'build_finished_at': job.finished_at,
        })

    return updated_fields


def callback(body, routing_key, headers, **_):
    """Handle a single message."""
    status, duration = body['build_status'], body['build_duration']
    LOGGER.info('Got message for job_id: %i, status: "%s", duration: %s',
                body['build_id'], status, duration)

    METRIC_JOB_DURATION.labels(status, body['build_stage']).observe(duration or 0)

    if status == 'running' and duration > 5:
        # Suspicious message, compare against the API and send if different.
        new_body = update_stale_webhook(body)
        if not new_body:
            return

        LOGGER.info(' - Resending message for %s', routing_key)
        QUEUE.send_message(new_body, routing_key,
                           exchange=RABBITMQ_EXCHANGE_DST,
                           headers=headers)


def main():
    """Consume webhooks from the queue."""
    QUEUE.consume_messages(RABBITMQ_EXCHANGE_SRC, ['gitlab.com.#.build'], callback,
                           queue_name=RABBITMQ_QUEUE)
