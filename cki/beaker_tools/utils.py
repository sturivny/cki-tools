"""CKI Beaker Tools Utils."""


def decode_test_result(status, result, skipped):
    """
    Get result for the combination of status, result and skipped.

    Translate Beaker specific status/result into one of the typified
    kcidb test status: ["ERROR", "FAIL", "PASS", "DONE", "SKIP"]

    Using the 'skipped' flag, identify tests that did not run, independently
    from what status or result contain.
    """
    if skipped:
        return 'SKIP'

    # Check if the test finished.
    if status not in ('Completed', 'Aborted'):
        return 'SKIP'

    # Check the result.
    if result == 'Skip':
        return 'SKIP'
    if result == 'Pass':
        return 'PASS'
    if result in ('Fail', 'Panic', 'Warn'):
        return 'FAIL'

    return 'ERROR'
