"""Get a list of hosts to avoid for CKI Beaker Jobs."""
import argparse
import base64
import datetime
import os
import pathlib
import subprocess
import tempfile

from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.psql import PSQLHandler
import sentry_sdk

from cki.cki_tools import _utils

BUCKET_CONFIG_NAME = os.environ.get('BUCKET_CONFIG_NAME')
LIST_PATH = os.environ.get('LIST_PATH', 'broken-machines-list.txt')

KRB_PRINCIPAL = os.environ.get('TEIID_KEYTAB_PRINCIPAL')
KRB_KEYTAB = os.environ.get('TEIID_KEYTAB_CONTENT_BASE64')

THRESHOLD_RECIPES_RUN = misc.get_env_int('THRESHOLD_RECIPES_RUN', 15)
THRESHOLD_BROKEN = os.environ.get('THRESHOLD_BROKEN', 0.7)
DAYS_TO_CHECK = misc.get_env_int('DAYS_TO_CHECK', 7)
LIMIT_RESULTS = misc.get_env_int('LIMIT_RESULTS', 50)

LOGGER = get_logger('cki.beaker_tools.broken_machines')


def upload_list(hosts):
    """Upload list of broken machines to s3 bucket."""
    bucket = _utils.S3Bucket.from_bucket_string(os.environ[BUCKET_CONFIG_NAME])
    file_content = '\n'.join(hosts)

    bucket.client.put_object(
        Bucket=bucket.spec.bucket,
        Key=bucket.spec.prefix + LIST_PATH,
        Body=file_content,
        ContentType='text/plain',
    )


def update_list(days, threshold_broken, threshold_recipes_run, limit):
    """Update list of broken machines."""
    db_handler = PSQLHandler(
        os.environ.get('TEIID_HOST', 'localhost'),
        os.environ.get('TEIID_PORT', '5433'),
        os.environ.get('TEIID_DATABASE', 'public'),
        require_ssl=True
    )

    since = (
        datetime.date.today() - datetime.timedelta(days=days)
    )

    query = '''
        SELECT
            abrt.fqdn,
            abrt.count / cast(ran.count as float)
        FROM
            (
                SELECT
                    recipe_resource.fqdn,
                    count(recipe_resource.fqdn) AS count
                FROM
                    recipe
                LEFT JOIN recipe_resource ON
                    recipe.id = recipe_resource.recipe_id
                WHERE
                    recipe.status = 'Aborted' AND
                    recipe.start_time > %(since)s
                GROUP BY
                    recipe_resource.fqdn
                HAVING
                    count(recipe_resource.fqdn) > 0
            ) AS abrt,
            (
                SELECT
                    recipe_resource.fqdn,
                    count(recipe_resource.fqdn) AS count
                FROM
                    recipe
                LEFT JOIN recipe_resource ON
                    recipe.id = recipe_resource.recipe_id
                WHERE
                    recipe.start_time > %(since)s
                GROUP BY
                    recipe_resource.fqdn
                HAVING
                    count(recipe_resource.fqdn) > 0
            ) AS ran
        WHERE
            ran.fqdn = abrt.fqdn
        GROUP BY
            abrt.fqdn,
            abrt.count,
            ran.count
        HAVING
            abrt.count/ran.count > %(threshold_broken)s AND
            ran.count > %(threshold_recipes_run)s
        LIMIT %(limit)s;
    '''

    data = {
        'since': since,
        'threshold_broken': threshold_broken,
        'threshold_recipes_run': threshold_recipes_run,
        'limit': limit,
    }

    rows = db_handler.execute(query, data)

    hosts = []
    for fqdn, ratio in rows:
        hosts.append(fqdn)
        LOGGER.info('%s ratio: %s', fqdn, ratio)

    if misc.is_production():
        upload_list(hosts)


def main():
    """Do everything (parse args, get data from db, create list)."""
    misc.sentry_init(sentry_sdk)
    parser = argparse.ArgumentParser(
        description='Get possible broken machines'
    )
    parser.add_argument(
        '--threshold-recipes-run',
        help='Minimum recipes run',
        default=THRESHOLD_RECIPES_RUN,
        type=int,
    )
    parser.add_argument(
        '--threshold-broken',
        help='Threshold for broken machines',
        default=float(THRESHOLD_BROKEN),
        type=float,
    )
    parser.add_argument(
        '--days',
        help='Days used in the query',
        default=DAYS_TO_CHECK,
        type=int,
    )
    parser.add_argument(
        '--limit',
        help='Limit results',
        default=LIMIT_RESULTS,
        type=int,
    )
    args = parser.parse_args()

    with tempfile.TemporaryDirectory() as tempdirname:
        if KRB_PRINCIPAL:
            os.environ['KRB5CCNAME'] = pathlib.Path(tempdirname, 'cache').as_posix()
            keytab_path = pathlib.Path(tempdirname, 'keytab')
            keytab_path.write_bytes(base64.b64decode(KRB_KEYTAB))
            subprocess.run(['kinit', '-kt', keytab_path.as_posix(), KRB_PRINCIPAL], check=True)

        update_list(
            args.days,
            args.threshold_broken,
            args.threshold_recipes_run,
            args.limit
        )


if __name__ == '__main__':
    main()
