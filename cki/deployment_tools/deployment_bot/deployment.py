"""Handle a deployment."""
import os

from cki_lib import messagequeue
from cki_lib import metrics
from cki_lib import misc
from cki_lib.gitlab import get_instance
from cki_lib.logger import get_logger
from cki_lib.session import get_session
import sentry_sdk

from . import matcher
from . import utils

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)
RABBITMQ_KEEPALIVE_S = misc.get_env_int('RABBITMQ_KEEPALIVE_S', 60)
IRC_BOT_EXCHANGE = os.environ.get('IRC_BOT_EXCHANGE')


class Deployment:
    """Handle deployments."""

    def __init__(self, config):
        """Initialize a deployment instance."""
        self.config = config
        self.messagequeue = messagequeue.MessageQueue(keepalive_s=RABBITMQ_KEEPALIVE_S)

    def listen(self):
        """Run the listening loop."""
        misc.sentry_init(sentry_sdk)
        metrics.prometheus_init()

        self.messagequeue.consume_messages(
            os.environ.get('WEBHOOK_RECEIVER_EXCHANGE', 'cki.exchange.webhooks'),
            os.environ['DEPLOYMENT_BOT_ROUTING_KEYS'].split(),
            self.callback,
            queue_name=os.environ.get('DEPLOYMENT_BOT_QUEUE'))

    @staticmethod
    def set_environment_external_url(environment_url, external_url):
        """Set the external environment URL for a job."""
        instance_url, path_with_namespace, environment_id = utils.parse_environment_url(
            environment_url)
        with get_instance(instance_url) as gl_instance:
            gl_project = gl_instance.projects.get(path_with_namespace, lazy=True)
            gl_environment = gl_project.environments.get(environment_id)
            LOGGER.info('Setting deployment URL for %s to %s', environment_url, external_url)
            if misc.is_production():
                gl_environment.external_url = external_url
                gl_environment.save()
            else:
                LOGGER.info('Not set without production mode')

    @staticmethod
    def dotenv(project_url, job_ids, file_name, env_name):
        """Search dotenv-formatted artifacts for a value."""
        instance_url, path_with_namespace = utils.parse_project_url(project_url)
        with get_instance(instance_url) as gl_instance:
            gl_project = gl_instance.projects.get(path_with_namespace, lazy=True)
            for job_id in job_ids:
                try:
                    gl_job = gl_project.jobs.get(job_id, lazy=True)
                    contents = gl_job.artifact(file_name).decode('utf8').strip()
                    for line in contents.split('\n'):
                        key, value = line.split('=', 1)
                        if key == env_name:
                            return value
                # pylint: disable=broad-except
                except Exception:
                    # expected
                    pass
        return None

    @staticmethod
    def pipeline_variables(body):
        """Return the pipeline variables from a webhook message."""
        return {variable['key']: variable['value']
                for variable in body['object_attributes']['variables']}

    def handle_deployment_pipeline_update(self,
                                          deployment_project_url,
                                          deployment_job_ids,
                                          deployment_pipeline_status,
                                          deployment_pipeline_variables):
        """Process status updates about deployment pipelines."""
        log_message = deployment_pipeline_variables.get(
            f'deployment_bot_message_{deployment_pipeline_status}')
        if not log_message:
            return False

        LOGGER.info('Handling deployment pipeline')

        environment_url = deployment_pipeline_variables.get('deployment_bot_environment_url')
        external_url = deployment_pipeline_variables.get('deployment_bot_external_url', '')
        if external_url.startswith('dotenv:'):
            external_url = Deployment.dotenv(deployment_project_url, deployment_job_ids,
                                             *external_url.split(':')[1:])
        if environment_url and external_url:
            Deployment.set_environment_external_url(environment_url, external_url)

        LOGGER.info('%s', log_message)
        if misc.is_production() and IRC_BOT_EXCHANGE:
            self.messagequeue.send_message(
                {'message': log_message}, 'irc.message', IRC_BOT_EXCHANGE)
        return True

    @staticmethod
    def trigger_deployment_pipeline(project_url, project_config, deployment_variables):
        """Kick off a deployment pipeline."""
        LOGGER.info('Handling deployment of %s', project_url)
        deployment_instance_url, deployment_path_with_namespace = utils.parse_project_url(
            project_config['.deployment']['project_url'])

        with get_instance(deployment_instance_url) as gl_instance:
            LOGGER.info('Triggering deployment of %s with %s', project_url, deployment_variables)

            if misc.is_production():
                gl_project = gl_instance.projects.get(deployment_path_with_namespace, lazy=True)
                gl_pipeline = gl_project.pipelines.create({
                    'ref': project_config['.deployment'].get('ref', 'main'),
                    'variables': [{'key': k, 'value': v} for k, v in deployment_variables.items()]})
                LOGGER.info('Triggered deployment via %s', gl_pipeline.web_url)
            else:
                LOGGER.info('Not triggered without production mode')

    def callback(self, body=None, **_):
        """Handle a webhook message."""
        if 'project' in body:
            project_url = body['project']['web_url']
        else:
            project_url = body['repository']['homepage']

        # handle messages about the actual deployment pipelines
        if (body.get('object_kind') == 'pipeline' and
                self.handle_deployment_pipeline_update(project_url,
                                                       [j['id'] for j in body['builds']],
                                                       body['object_attributes']['status'],
                                                       Deployment.pipeline_variables(body))):
            return

        # handle messages that are configured to trigger deployment pipelines
        for project_config in self.config.values():
            deployment_variables = matcher.Matcher(project_config, project_url, body).match()
            if deployment_variables:
                self.trigger_deployment_pipeline(project_url, project_config, deployment_variables)
                return

        LOGGER.info('Ignoring unknown message for %s', project_url)
