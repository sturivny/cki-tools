"""Get messages from DW and forward them to upstream KCIDB."""
import argparse
import os

from cki_lib import metrics
from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.messagequeue import MessageQueue
import kcidb
import prometheus_client as prometheus
import sentry_sdk

LOGGER = get_logger('cki.kcidb.forward_upstream')

IS_PRODUCTION = misc.is_production()
CONVERT_TO_V3 = misc.get_env_bool('KCIDB_CONVERT_TO_V3', False)
MAX_BATCH_SIZE = misc.get_env_int('MAX_BATCH_SIZE', 500)

KCIDB_PROJECT_ID = os.environ.get('KCIDB_PROJECT_ID')
KCIDB_TOPIC_NAME = os.environ.get('KCIDB_TOPIC_NAME')

RABBITMQ_PASSWORD = os.environ.get('RABBITMQ_PASSWORD')
RABBITMQ_EXCHANGE = os.environ.get('RABBITMQ_EXCHANGE')
RABBITMQ_QUEUE = os.environ.get('RABBITMQ_QUEUE')
RABBITMQ_TIMEOUT_S = misc.get_env_int('RABBITMQ_TIMEOUT_S', 30)
RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST')
RABBITMQ_PORT = misc.get_env_int('RABBITMQ_PORT', 5672)
RABBITMQ_USER = os.environ.get('RABBITMQ_USER')

METRIC_OBJECT_BUFFERED = prometheus.Histogram(
    'cki_kcidb_object_buffered', 'Number of KCIDB objects buffered for forwarding')
METRIC_OBJECT_FORWARDED = prometheus.Counter(
    'cki_kcidb_object_forwarded', 'Number of KCIDB objects forwarded')


class KCIDBMessage:
    """A KCIDB message."""

    def __init__(self, kcidb_client, dry_run=False):
        """Create an object."""
        self.kcidb_client = kcidb_client
        self.dry_run = dry_run
        self.checkouts = []
        self.builds = []
        self.tests = []
        self.version = None
        self.msg_ack_callbacks = []

    def add(self, obj_type, obj, ack_fn):
        """Add the object to the correct list."""
        LOGGER.debug('Adding to KCIDBMessage: "%s" (%s)', obj_type, obj)
        obj_lists = {
            'checkout': self.checkouts,
            'build': self.builds,
            'test': self.tests,
        }
        obj_lists[obj_type].append(obj)
        self.msg_ack_callbacks.append(ack_fn)

        METRIC_OBJECT_BUFFERED.observe(len(self))

        if not self.version:
            self.version = misc.get_nested_key(obj, 'misc/kcidb/version')

        for key in ('kcidb', 'is_public'):
            try:
                del obj['misc'][key]
            except KeyError:
                pass

    def _sanitize_test(self):
        """Remove spaces from the test path."""
        for test in self.tests:
            path = test.get('path')
            if path:
                test['path'] = test['path'].replace(' ', '_')

    @staticmethod
    def _convert_to_v3(data):
        # pylint: disable=too-many-branches
        """Convert v4 schemed data into v3."""
        v3_data = {
            'version': {'major': 3, 'minor': 0},
            'revisions': [],
            'builds': [],
            'tests': []
        }

        for checkout in data.get('checkouts', []):
            checkout['id'] = checkout['git_commit_hash']

            if 'patchset_hash' in checkout:
                if checkout['patchset_hash']:
                    checkout['id'] += '+' + checkout['patchset_hash']
                checkout.pop('patchset_hash')

            if 'patchset_files' in checkout:
                checkout['patch_mboxes'] = checkout.pop('patchset_files')
            if 'start_time' in checkout:
                checkout['discovery_time'] = checkout.pop('start_time')
            if 'comment' in checkout:
                checkout['description'] = checkout.pop('comment')

            v3_data['revisions'].append(checkout)

        for build in data.get('builds', []):
            build['revision_id'] = build['misc']['revision_id']

            if 'checkout_id' in build:
                build.pop('checkout_id')

            if 'comment' in build:
                build['description'] = build.pop('comment')

            v3_data['builds'].append(build)

        for test in data.get('tests', []):
            if 'comment' in test:
                test['description'] = test.pop('comment')

            if 'environment' in test:
                if 'comment' in test['environment']:
                    test['environment']['description'] = test['environment'].pop('comment')

            v3_data['tests'].append(test)

        kcidb.io.schema.validate(v3_data)
        return v3_data

    @property
    def encoded(self):
        """Return the KCIDB message object."""
        self._sanitize_test()
        data = {
            'version': self.version,
            'checkouts': self.checkouts,
            'builds': self.builds,
            'tests': self.tests,
        }

        if CONVERT_TO_V3:
            data = self._convert_to_v3(data)

        kcidb.io.schema.validate(data)

        return data

    @property
    def has_objects(self):
        """Return true if any of the lists has elements."""
        return len(self) > 0

    def clear(self):
        """Remove all elements."""
        LOGGER.debug('Clearing KCIDBMessage elements')
        self.checkouts = []
        self.builds = []
        self.tests = []
        self.version = None
        self.msg_ack_callbacks = []

    def ack_messages(self):
        """Ack all messages contained on this object."""
        LOGGER.debug('Acking %i messages', len(self.msg_ack_callbacks))
        for ack_fn in self.msg_ack_callbacks:
            ack_fn()

    def submit(self):
        """Upload message to upstream kcidb."""
        LOGGER.debug('Submitting KCIDBMessage elements')
        if not self.dry_run:
            self.kcidb_client.submit(self.encoded)
            LOGGER.info('%i elements submitted', len(self))
            METRIC_OBJECT_FORWARDED.inc(amount=len(self))
        else:
            LOGGER.info('Skipping submit. Dry Run')
        self.ack_messages()
        self.clear()

    def __len__(self):
        """Return number of elements."""
        return (
            len(self.checkouts) +
            len(self.builds) +
            len(self.tests)
        )


def callback(message, body=None, ack_fn=None, **_):
    """Process one received message."""
    LOGGER.debug('Message Received: %s', body)

    if not body:
        if message.has_objects:
            LOGGER.debug('Calling message.submit')
            message.submit()
        return

    LOGGER.info('Got message for %s iid=%s', body['object_type'], body['iid'])

    status = body['status']
    if status not in ('new', 'updated'):
        LOGGER.info('Message status is not "new" or "updated". Skipping (status: %s)', status)
        ack_fn()
        return

    is_public = misc.get_nested_key(body, 'object/misc/is_public')
    if not is_public:
        LOGGER.info('Message is not public. Skipping (is_public: %s)', is_public)
        ack_fn()
        return

    if body['object_type'] == 'checkout' and not body['object'].get('git_repository_url'):
        # Brew builds have a fake value as git_commit_hash
        # Remove it to avoid collisions upstream
        if misc.get_nested_key(body, 'object/git_commit_hash'):
            del body['object']['git_commit_hash']

    message.add(body['object_type'], body['object'], ack_fn)

    if len(message) > MAX_BATCH_SIZE:
        LOGGER.debug('Reached MAX_BATCH_SIZE, calling message.submit')
        message.submit()


def process_queue(args):
    """Listen for Data Warehouse KCIDB messages."""
    queue = MessageQueue(
        host=args.rabbitmq_host, port=args.rabbitmq_port,
        user=args.rabbitmq_user, password=args.rabbitmq_password,
    )

    kcidb_client = kcidb.Client(
        project_id=args.kcidb_project_id,
        topic_name=args.kcidb_topic_name
    )

    message = KCIDBMessage(kcidb_client, dry_run=not IS_PRODUCTION)

    queue.consume_messages(args.rabbitmq_exchange, ['#'],
                           lambda **kwargs: callback(message, **kwargs),
                           queue_name=args.rabbitmq_queue,
                           prefetch_count=0,
                           inactivity_timeout=args.rabbitmq_timeout_s,
                           return_on_timeout=False,
                           manual_ack=True)


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        description='Get messages from DW and forward them to upstream KCIDB.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--rabbitmq-exchange', default=RABBITMQ_EXCHANGE,
                        help='Defaults to env RABBITMQ_EXCHANGE.')
    parser.add_argument('--rabbitmq-queue', default=RABBITMQ_QUEUE,
                        help='Defaults to env RABBITMQ_QUEUE.')
    parser.add_argument('--rabbitmq-host', default=RABBITMQ_HOST,
                        help='Defaults to env RABBITMQ_HOST.')
    parser.add_argument('--rabbitmq-port', type=int, default=RABBITMQ_PORT,
                        help='Defaults to env RABBITMQ_PORT.')
    parser.add_argument('--rabbitmq-user', default=RABBITMQ_USER,
                        help='Defaults to env RABBITMQ_USER.')
    parser.add_argument('--rabbitmq-password', default=RABBITMQ_PASSWORD,
                        help='Defaults to env RABBITMQ_PASSWORD.')
    parser.add_argument('--rabbitmq-timeout-s', default=RABBITMQ_TIMEOUT_S,
                        help='Defaults to env RABBITMQ_TIMEOUT_S.')

    parser.add_argument('--kcidb-project-id', default=KCIDB_PROJECT_ID,
                        help='Defaults to env KCIDB_PROJECT_ID.')
    parser.add_argument('--kcidb-topic-name', default=KCIDB_TOPIC_NAME,
                        help='Defaults to env KCIDB_TOPIC_NAME.')

    return parser.parse_args()


def main():
    """Run the main CLI Interface."""
    misc.sentry_init(sentry_sdk)
    parsed_args = parse_args()

    metrics.prometheus_init()

    process_queue(parsed_args)


if __name__ == '__main__':
    main()
