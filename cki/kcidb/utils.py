"""Misc utility functions for kcidb."""
import hashlib
import mimetypes
import os
import re

from botocore.exceptions import ClientError
from cki_lib.gitlab import parse_gitlab_url
from cki_lib.logger import get_logger
from cki_lib.session import get_session
import dateutil

from cki.cki_tools import _utils

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)

BUCKET_PUBLIC_SPEC = os.environ.get('BUCKET_PUBLIC_SPEC')
BUCKET_PRIVATE_SPEC = os.environ.get('BUCKET_PRIVATE_SPEC')
BUCKETS = {
    'public': _utils.S3Bucket.from_bucket_string(
        BUCKET_PUBLIC_SPEC) if BUCKET_PUBLIC_SPEC else None,
    'private': _utils.S3Bucket.from_bucket_string(
        BUCKET_PRIVATE_SPEC) if BUCKET_PRIVATE_SPEC else None
}


def upload_file(visibility, artifacts_path, file_name, file_content=None,
                source_path=None):
    """
    Upload file to final storage.

    This method should decide where to put the file, and return the url
    where to access that file.
    """
    LOGGER.debug('Uploading %s', file_name)

    bucket = BUCKETS.get(visibility)
    if not bucket:
        LOGGER.debug('S3 bucket not configured. Skipping upload.')
        return None

    if not (file_content or source_path):
        LOGGER.warning('File "%s" is empty', file_name)
        return None

    file_path = os.path.join(bucket.spec.prefix, artifacts_path,
                             file_name)
    file_url = os.path.join(bucket.spec.endpoint, bucket.spec.bucket,
                            file_path)

    content_type, _ = mimetypes.guess_type(file_name, strict=False)
    content_type = content_type or 'text/plain'

    try:
        bucket.client.head_object(Bucket=bucket.spec.bucket,
                                  Key=file_path)
        LOGGER.debug('File %s already exists in remote', file_name)
    except ClientError:
        if file_content:
            bucket.client.put_object(Bucket=bucket.spec.bucket,
                                     Key=file_path, Body=file_content,
                                     ContentType=content_type)
        else:
            bucket.client.upload_file(Bucket=bucket.spec.bucket,
                                      Filename=source_path,
                                      Key=file_path,
                                      ExtraArgs={"ContentType": content_type})

    LOGGER.debug('File correctly uploaded to %s', file_url)
    return file_url


def is_empty(value):
    """Check if value is not empty."""
    return (
        value is None or
        value == [] or
        value == {}
    )


def timestamp_to_iso(timestamp):
    """Format timestamp as ISO. Adds tzinfo=UTC if no tzinfo present."""
    date = dateutil.parser.parse(timestamp)
    if not date.tzinfo:
        date = date.replace(tzinfo=dateutil.tz.UTC)
    return date.isoformat()


def tzaware_str2utc(str_date):
    """Convert tz-aware date string to UTC zulu.

    Arguments:
        str_date: str, date string like "2021-04-13T14:33:08-04:00" or "2021-04-13 10:30:07 +0000"
    Returns:
        str, e.g. 2021-04-13T18:33:08+00:00
    """
    return dateutil.parser.parse(str_date).astimezone(dateutil.tz.UTC).isoformat()


def parse_selftests(selftest_results, architecture):
    """Get test_data for kselftests if present."""
    selftests = []
    for index, line in enumerate(selftest_results.strip().split('\n')):
        test_name, return_code = line.split(': ')
        sanitized_test_name = test_name.replace(' ', '_')
        test_data = {"CKI_NAME": f"kselftest build - {test_name}",
                     "CKI_UNIVERSAL_ID":
                     f"kselftest-build.{sanitized_test_name}",
                     "return_code": int(return_code),
                     "test_index": index,
                     "arch": architecture}
        selftests.append(test_data)

    return selftests


def _get_original_pipeline(pipeline):
    """Get the original pipeline this result points to."""
    original_pipeline_url = pipeline.variables.get('original_pipeline_url')
    if original_pipeline_url:
        _, original_pipeline = parse_gitlab_url(original_pipeline_url)
    else:
        original_pipeline = pipeline.project.pipelines.get(
            pipeline.variables['original_pipeline_id']
        )

    return original_pipeline


def patch_list_hash(patch_list):
    """Get hash of a list of patches."""
    if not patch_list:
        return ''

    patch_hash_list = []
    for patch_url in patch_list:
        # Transform /mbox/ url into /raw/ to get the patch diff only.
        # Patchwork mbox includes headers that can change after people reply to the patches.
        patch_url = re.sub(r'/mbox/?$', '/raw/', patch_url)

        patch = SESSION.get(patch_url)
        patch_hash_list.append(
            hashlib.sha256(patch.content).hexdigest()
        )

    merged_hashes = '\n'.join(patch_hash_list) + '\n'
    return hashlib.sha256(merged_hashes.encode('utf8')).hexdigest()
