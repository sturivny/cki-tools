# Testing the gitlab-ci-bot

The gitlab-ci-bot has integration tests that must be run before merging any changes.

To run them, you need to have access to a https://gitlab.com account that is not member of the CKI team.
Then, define the following environment variables:

- `IT_GITLAB_MR_URL`: https://gitlab.com
- `IT_GITLAB_MR_MEMBER_TOKEN`: your personal access token at https://gitlab.com
- `IT_GITLAB_MR_CONTRIBUTOR_TOKEN`: a personal access token for the non-member
- `IT_GITLAB_MR_PROJECT_NAME`: cki-project/pipeline-definition
- `IT_GITLAB_MR_PROJECT_URL`: https://gitlab.com/cki-project/pipeline-definition
- `IT_GITLAB_PIPELINES_URL`: https://gitlab.com
- `IT_GITLAB_PIPELINES_TOKEN`: your personal access token at https://gitlab.com
- `IT_GITLAB_PIPELINES_PROJECT_CKI`: `redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/external-triggers`
- `IT_GITLAB_PIPELINES_PROJECT_BREW`: `redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/brew-builds`

You can run the integration tests with

```shell
# run all tests
python3 -m unittest inttests.test_gitlab_ci_bot -v
# run an individual test
python3 -m unittest inttests.test_gitlab_ci_bot.TestPipelineBot.test_member -v
```
