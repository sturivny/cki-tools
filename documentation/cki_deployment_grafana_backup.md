# `shell-scripts/cki_deployment_grafana_backup.sh`

Backup a Grafana instance by creating dumps via `cki.deployment_tools.grafana`.

```shell
cki_deployment_grafana_backup.sh <group> <count>
```

The last `<count>` backups per group `<group>` are kept.

The following `crontab` schedule would perform daily, weekly, monthly and
yearly backups:

```plain
@daily   cki_deployment_grafana_backup.sh daily    7
@weekly  cki_deployment_grafana_backup.sh weekly   4
@monthly cki_deployment_grafana_backup.sh monthly 12
@yearly  cki_deployment_grafana_backup.sh yearly   2
```

## Environment variables

| Field                | Type   | Required | Description                                               |
|----------------------|--------|----------|-----------------------------------------------------------|
| `GRAFANA_URL`        | string | yes      | Grafana instance URL                                      |
| `GRAFANA_TOKEN`      | string | yes      | Grafana secret token                                      |
| `BUCKET_CONFIG_NAME` | string | yes      | Name of an environment variable with bucket configuration |
