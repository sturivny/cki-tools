# Terminate orphaned EC2 instances

`cki.cki_tools.orphan_hunter_ec2`

Tool to find and terminate orphaned
- EC2 instances
- EC2 spot instance requests

## Usage

```shell
Usage: python3 -m cki.cki_tools.orphan_hunter_ec2
    [--untagged-key-prefix UNTAGGED_KEY_PREFIX]
    [--untagged-age UNTAGGED_AGE]
    [--runaway-key-prefix RUNAWAY_KEY_PREFIX]
    [--runaway-age RUNAWAY_AGE]
    [--spot-key-prefix SPOT_KEY_PREFIX]
    [--spot-age SPOT_AGE]
    [--irc-bot-exchange IRC_BOT_EXCHANGE]
    [--summary-only]
```

### EC2 instances

In a first step, a list of all running and stopped EC2 instances is
determined. Instances on this list are then terminated if they fulfil one of
the following sets of conditions:

- *untagged instances*:
    - `--untagged-key-prefix` is specified
    - instances have a matching EC2 key associated
    - instances are older than `--untagged-age` (default 1h)
    - instances do not have any tags
- *runaway instances*:
    - `--runaway-key-prefix` is specified
    - instances have a matching EC2 key associated
    - instances are older than `--runaway-age` (default 1w)

Instances are only terminated in production mode.

### EC2 spot instance requests

In a first step, a list of all open EC2 spot instance requests is
determined. Requests on this list are then canceled if they fulfil one of
the following sets of conditions:

- *old requests*:
    - `--spot-key-prefix` is specified
    - instances have a matching EC2 key associated
    - instances are older than `--spot-age` (default 1h)

Spot instance requests are only terminated in production mode.

## Configuration

| Environment variable         | Type     | Secret | Description                                         |
|------------------------------|----------|--------|-----------------------------------------------------|
| `CKI_DEPLOYMENT_ENVIRONMENT` | string   | no     | Only terminate instances when set to `production`   |
| `IRC_BOT_EXCHANGE`           | string   | no     | AMQP exchange for notifications about termination   |
| `RABBITMQ_HOST`              | url      | no     | AMQP host                                           |
| `RABBITMQ_PORT`              | int      | no     | AMQP port, TLS is used for port 443                 |
| `RABBITMQ_USER`              | string   | no     | AMQP user                                           |
| `RABBITMQ_PASSWORD`          | string   | yes    | AMQP password                                       |
| `RABBITMQ_CAFILE`            | filename | no     | AMQP CA file path                                   |
| `RABBITMQ_CERTFILE`          | filename | yes    | AMQP certificate + private key file path            |
| `RABBITMQ_KEEPALIVE_S`       | int      | no     | Time to keep AMQP connection alive between messages |
