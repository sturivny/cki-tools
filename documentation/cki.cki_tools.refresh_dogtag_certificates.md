# Dogtag certificate manager

`cki.cki_tools.refresh_dogtag_certificates`

Tool to download and refresh [Dogtag]-based certificates via the following steps:

1. download current production certificates from S3 bucket
1. retrieve certificates from the Dogtag server
1. refresh certificates if needed
1. propose revoking certificates (but must be done manually)
1. upload updated production certificates to S3 bucket

## Usage

```shell
Usage: python3 -m cki.cki_tools.refresh_dogtag_certificates
    [--dogtag-url DOGTAG_URL]
    [--refresh-after INTERVAL]
    [--refresh-before INTERVAL]
    [--expire-after INTERVAL]
    [--expire-before INTERVAL]
    [--s3-url S3_URL]
    [--download-only FILE]
```

With `--download-only`, only download current production certificates from the
S3 bucket from `--s3-url` and write them to the given file.

Otherwise, after downloading from S3, contact the Dogtag server from
`--dogtag-url` for all environment variables ending in `_PRIVATE_KEY` (see
below). If any of `--refresh-after` or `--refresh-before` is specified (via e.g. `100 days`),
automatically refresh certificates. If any of `--expire-after` or
`--expire-before` is specified, print a list of certificates that can be
(manually) revoked. At the end, upload the newest certificates back to the S3
bucket.

## Configuration

| Environment variable         | Type   | Secret | Description                                                          |
|------------------------------|--------|--------|----------------------------------------------------------------------|
| `CKI_DEPLOYMENT_ENVIRONMENT` | string | no     | Only refreshes certificates when set to `production`                 |
| `DOGTAG_URL`                 | string | no     | REST API URL of the Dogtag server                                    |
| `DOGTAG_S3_URL`              | string | no     | S3 URL to store certificates like <https://endpoint/bucket/file.yml> |
| `*_PRIVATE_KEY`              | string | yes    | Private key for the certificates                                     |
| `*_USER`                     | string | no     | User accounts for the certificates                                   |
| `*_PASSWORD`                 | string | yes    | Passwords for the certificates                                       |

[Dogtag]: https://www.dogtagpki.org/
