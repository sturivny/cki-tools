# `cki.cki_tools.url_shortener`

Provide an URL shortener with S3 backing storage.

A new short URL can be generated by something like

```bash
curl \
    --request POST \
    --header "Authorization: Bearer $TOKEN" \
    --data '{"url": "http://example.url/"}' \
    https://shortener-url
```

The short URL is returned as plain text.

## Configuration

| Environment variable         | Description                                                                 |
|------------------------------|-----------------------------------------------------------------------------|
| `CKI_DEPLOYMENT_ENVIRONMENT` | Define the deployment environment (production/staging)                      |
| `URL_SHORTENER_URL`          | External URL of the shortener used during short URL generation              |
| `URL_SHORTENER_TOKEN`        | Secret token that needs to be provided via `Authorization: Bearer <token> ` |
| `URL_SHORTENER_BUCKET_SPEC`  | S3 bucket to use as a backing store                                         |
| `SENTRY_SDN`                 | Sentry SDN                                                                  |

### `CKI_DEPLOYMENT_ENVIRONMENT`

On staging developments (`CKI_DEPLOYMENT_ENVIRONMENT != production`), no short
URLs are generated.
