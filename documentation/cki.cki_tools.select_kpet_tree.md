# `cki.cki_tools.select_kpet_tree`

Determine the correct kpet tree for a tree family and the kernel NVR.

```shell
python3 -m cki.cki_tools.select_kpet_tree tree_family nvr
```

For some tree families like `rhel7`, multiple kpet trees with specific
requirements exist. The correct tree is selected by mapping tree family and
kernel NVR to a kpet tree as defined in the `mapping` variable inside
`select_kpet_tree`.
